import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/documents'
import PendingListItem from './components/PendingListItem'
import EditContainer from './PendingEdit'
import CreateContainer from './New'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.documents
      .getItems(state)
      .filter(d => d.documentTypeId === null),
    params: store.select.documents.getParams(state),
    resourceName: {
      ...resourceName,
      ...{ plural: 'Pending Files', route: '/pending-files' }
    },
    resourceList,
    detailView: EditContainer,
    createView: CreateContainer,
    navView: 'container/documents',
    loading: state.loading.effects.documents.fetchPending || false,
    disableAction: true
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.documents.fetchPending()],
    refetch: () => dispatch.documents.fetchPending(),
    changeParams: filters => dispatch.documents.changeParams(filters),
    resetParams: () => dispatch.documents.resetParams(),
    removeAction: id => dispatch.documents.removePending(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
