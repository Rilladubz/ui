import React, { Component } from 'react'
import { List } from 'semantic-ui-react'

import EmptyState from './EmptyState'

class ActivityTab extends Component {
  renderList = () => {
    const { activities } = this.props
    const items = activities.map((v, k) => <List.Item content={v.id} />)
    return <List>{items}</List>
  }

  render () {
    const { activities } = this.props
    return (
      <div>
        {activities.length ? this.renderList() : <EmptyState name='Activity' />}
      </div>
    )
  }
}

export default ActivityTab
