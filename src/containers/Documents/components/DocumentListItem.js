import React, { Fragment } from 'react'
import { Grid, Header, Icon } from 'semantic-ui-react'

import { getFontAwesomeIconFromMIME } from 'utils/fileIcons'
import Ellipsis from 'react-lines-ellipsis'

const DocumentListItem = ({ item, index, dependencies }) => {
  const { documentTypes } = dependencies
  const docTypeName = documentTypes
    ? documentTypes[`${item.documentTypeId}`].name
    : null
  return (
    <Grid columns={2} verticalAlign='middle'>
      <Grid.Column textAlign='left' width={2}>
        <Icon size='big' name={getFontAwesomeIconFromMIME(item.mimeType)} />
      </Grid.Column>
      <Grid.Column textAlign='left' width={14}>
        <Header as='h4' sub color='blue'>
          {docTypeName}
        </Header>
        <Header as='h5'>
          <Ellipsis
            text={`${item.filename}.${item.extension}`}
            basedOn='letters'
            trimRight
          />
        </Header>
      </Grid.Column>
    </Grid>
  )
}

export default DocumentListItem
