import React from 'react'
import { Grid, Header, Icon } from 'semantic-ui-react'

import { getFontAwesomeIconFromMIME } from 'utils/fileIcons'
import Ellipsis from 'react-lines-ellipsis'

const PendingListItem = ({ item, index }) => {
  return (
    <Grid columns={2} verticalAlign='middle'>
      <Grid.Column textAlign='left' width={2}>
        <Icon size='big' name={getFontAwesomeIconFromMIME(item.mimeType)} />
      </Grid.Column>
      <Grid.Column textAlign='left' width={12}>
        <Header as='h4' sub color='blue'>
          {item.extension}
        </Header>
        <small>
          <Ellipsis text={`${item.filename}`} basedOn='letters' trimRight />
        </small>
      </Grid.Column>
    </Grid>
  )
}

export default PendingListItem
