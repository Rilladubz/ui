import React, { Component } from 'react'
import { List, Tab, Menu, Header, Grid } from 'semantic-ui-react'
import { Steps, Icon, Button, message, Tabs } from 'antd'
import EmptyState from './EmptyState'

const Step = Steps.Step
const TabPane = Tabs.TabPane
const activityTypeMap = {
  userTask: { name: 'User Task', icon: 'user' }
}

class ProcessTab extends Component {
  state = {
    activeItem: 0
  }

  next () {
    const activeItem = this.state.activeItem + 1
    this.setState({ activeItem })
  }

  prev () {
    const activeItem = this.state.activeItem - 1
    this.setState({ activeItem })
  }

  handleItemClick = id => this.setState({ activeItem: id })

  componentDidMount () {
    const { processes, services } = this.props
    if (processes && processes.count()) {
      const id = processes.getIn([0, 'processInstanceId'])
      services.fetch(id)
      services.fetchActivityTree(id)
      services.fetchVariables(id)
    }
  }

  renderList = () => {
    const { processes, workflows } = this.props
    const { activeItem } = this.state
    const items = processes
      .map((v, k) => (
        <List.Item
          key={k}
          active={activeItem === k}
          onClick={() => this.handleItemClick(k)}
          content={workflows.getIn([`${v.workflowId}`, 'name'])}
        />
      ))
      .toJS()

    const activityTree = processes.getIn([activeItem, 'activity'])
    // mocking with some fake data now, need to get API working for real data
    const activityList = [
      {
        key: 'processDefintion',
        completed: true,
        title: 'test1',
        description: 'cats',
        icon: 'play'
      },
      {
        key: 'processDefintion',
        completed: true,
        title: 'test2',
        description: 'ducks',
        icon: 'play'
      },
      {
        key: 'processDefintion',
        completed: false,
        title: 'test3',
        description: 'bats',
        icon: 'play'
      }
    ]
    if (activityTree) {
      activityList.push({
        key: 'processDefintion',
        completed: true,
        title: activityTree.get('activityName'),
        description: 'Process Start',
        icon: 'play'
      })
      activityTree.get('childActivityInstances').forEach((c, k) => {
        activityList.push({
          key: c.get('activityId'),
          title: c.get('activityName'),
          description: activityTypeMap[c.get('activityType')].name,
          icon: activityTypeMap[c.get('activityType')].icon
        })
      })
      activityList[activityList.length - 1].active = true
    }
    return (
      <div>
        <Steps>
          {activityList.map((v, k) => {
            if (v.completed) {
              return <Step status='finish' title={v.title} icon={v.icon} />
            }
            return (
              <Step
                status='process'
                title={v.title}
                icon={<Icon type='loading' />}
              />
            )
          })}
        </Steps>
        <div className='steps-content' style={{ minHeight: 200 }}>
          {activityList[activeItem].description}
        </div>
        <div className='steps-action'>
          {activeItem < activityList.length - 1 && (
            <Button type='primary' onClick={() => this.next()}>
              Next
            </Button>
          )}
          {activeItem === activityList.length - 1 && (
            <Button
              type='primary'
              onClick={() => message.success('Processing complete!')}
            >
              Done
            </Button>
          )}
          {activeItem > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
        </div>
      </div>
    )
  }

  render () {
    const { processes, loading } = this.props
    return processes.count() ? (
      this.renderList()
    ) : (
      <EmptyState message='No processes linked to this document.' />
    )
  }
}

export default ProcessTab
