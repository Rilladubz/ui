import React from 'react'
import { Header } from 'semantic-ui-react'
import Ellipsis from 'react-lines-ellipsis'

const RolesListItem = ({ item, index, dependencies }) => {
  return (
    <Header as='h5'>
      <Ellipsis text={item.name} basedOn='letters' trimRight />
    </Header>
  )
}

export default RolesListItem
