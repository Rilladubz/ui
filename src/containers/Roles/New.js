import { connect } from 'react-redux'

import { ResourceCreateView } from 'components/Views'
import { modelConfig } from 'models/roles'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      permissions: state.permissions
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    navView: 'container/settings',
    loading: state.loading.effects.permissions.fetchAll,
    saving: state.loading.effects.roles.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveAction: role => dispatch.roles.create(role),
    prefetch: [() => dispatch.permissions.fetchAll()]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceCreateView)
