import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/roles'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/RolesListItem'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.roles.getItems(state),
    params: state.roles.get('params'),
    dependencies: {
      permissions: state.permissions.get('list')
    },
    resourceName,
    resourceList,
    navView: 'container/settings',
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    loading: state.loading.effects.roles.fetchAll
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.roles.fetchAll()],
    changeParams: filters => dispatch.roles.changeParams(filters),
    refetch: ops => dispatch.roles.fetchAll(ops),
    removeAction: id => dispatch.roles.remove(id),
    resetParams: () => dispatch.roles.resetParams()
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
