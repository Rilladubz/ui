import { connect } from 'react-redux'

import { ResourceCreateView } from 'components/Views'
import { modelConfig } from 'models/permissions'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    resourceName,
    resourceForm,
    navView: 'container/settings',
    saving: state.loading.effects.permissions.create || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  return {
    prefetch: [],
    saveAction: perm => dispatch.permissions.create(perm)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceCreateView)
