import { connect } from 'react-redux'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/permissions'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.permissions.getOne(state, { id: match.params.id }),
    resourceName,
    resourceForm,
    navView: 'container/settings',
    loading: state.loading.effects.permissions.fetch,
    saving: state.loading.effects.permissions.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: perm => dispatch.permissions.update(perm),
    prefetch: [() => dispatch.permissions.fetch({ id })]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceEditView)
