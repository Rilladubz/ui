import React, { Component } from 'react'
import {
  Grid,
  Segment,
  Feed,
  Table,
  Header,
  Divider,
  Tab,
  Label,
  Menu,
  Icon
} from 'semantic-ui-react'

const overdueTasks = () => (
  <Tab.Pane>
    <Table color='red'>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Task Name</Table.HeaderCell>
          <Table.HeaderCell>Document Type</Table.HeaderCell>
          <Table.HeaderCell>Overdue By</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Approval Process</Table.Cell>
          <Table.Cell>Invoice</Table.Cell>
          <Table.Cell>1 Day</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Review Process</Table.Cell>
          <Table.Cell>New Tenant Request</Table.Cell>
          <Table.Cell>2 Days</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  </Tab.Pane>
)

const incompleteTasks = () => (
  <Tab.Pane>
    <Table color='blue'>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Task Name</Table.HeaderCell>
          <Table.HeaderCell>Document Type</Table.HeaderCell>
          <Table.HeaderCell>Due In</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Approval Process</Table.Cell>
          <Table.Cell>Invoice</Table.Cell>
          <Table.Cell>1 Day</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Review Process</Table.Cell>
          <Table.Cell>New Tenant Request</Table.Cell>
          <Table.Cell>2 Days</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  </Tab.Pane>
)

class DashboardView extends Component {
  render () {
    const panes = [
      {
        menuItem: (
          <Menu.Item color='blue' key='incomplete'>
            <Icon name='wait' /> Incomplete <Label color='blue' content='2' />
          </Menu.Item>
        ),
        render: incompleteTasks
      },
      {
        menuItem: (
          <Menu.Item color='red' key='overdue'>
            <Icon name='warning circle' /> Overdue{' '}
            <Label color='red' content='2' />
          </Menu.Item>
        ),
        render: overdueTasks
      }
    ]
    return (
      <div style={{ padding: '2rem' }}>
        <Grid columns={1}>
          <Grid.Row>
            <Grid.Column width={16}>
              <Header as='h1'>My Dashboard</Header>
              <Divider section hidden />
              <Grid columns={2}>
                <Grid.Column width={10}>
                  <Header size='medium' icon='tasks' content='My Tasks' />
                  <Tab menu={{ attached: 'top' }} panes={panes} />
                </Grid.Column>
                <Grid.Column width={6}>
                  <Header size='medium' icon='feed' content='My Feed' />
                  <Segment>
                    <Feed>
                      <Feed.Event>
                        <Feed.Label icon='pencil' />
                        <Feed.Content
                          date='1 day ago'
                          summary='You uploaded a new Invoice.'
                        />
                      </Feed.Event>
                      <Feed.Event>
                        <Feed.Label icon='check circle' />
                        <Feed.Content
                          date='2 days ago'
                          summary='John Doe assigned an Approval task to you.'
                        />
                      </Feed.Event>
                      <Feed.Event>
                        <Feed.Label icon='pencil' />
                        <Feed.Content
                          date='3 days ago'
                          summary='You uploaded a new Tenant Registration.'
                        />
                      </Feed.Event>
                      <Feed.Event>
                        <Feed.Label icon='check circle' />
                        <Feed.Content
                          date='4 days ago'
                          summary='John Doe assigned an Review task to you.'
                        />
                      </Feed.Event>
                    </Feed>
                  </Segment>
                </Grid.Column>
              </Grid>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    )
  }
}

export default withNavigationViewController(DashboardView)
