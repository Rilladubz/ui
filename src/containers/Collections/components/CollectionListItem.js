import React, { Fragment } from 'react'
import { Header } from 'semantic-ui-react'
import Ellipsis from 'react-lines-ellipsis'

const CollectionListItem = ({ item, index, dependencies }) => {
  return (
    <Header as='h5'>
      <Ellipsis
        text={`${item.name || 'Untitled'}`}
        basedOn='letters'
        trimRight
      />
    </Header>
  )
}

export default CollectionListItem
