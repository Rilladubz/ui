import React, { Component } from 'react'
import { Form, List, Icon, Loader, Header, Grid } from 'semantic-ui-react'
import { getFontAwesomeIconFromMIME } from 'utils/fileIcons'
import Ellipsis from 'react-lines-ellipsis'

class DocumentPicker extends Component {
  state = {
    search: '',
    selected: [],
    types: null
  }

  componentWillMount () {
    this.props.fetchAction()
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      ...this.state,
      ...data
    })
  }

  toggleSelection = id => {
    const { selected } = this.state
    if (selected.includes(id)) {
      selected.splice(selected.indexOf(id), 1)
    } else {
      selected.push(id)
    }
    this.setState({
      selected
    })
  }

  render () {
    const { documents, documentTypes, onComplete, fetching } = this.props
    const { types, selected } = this.state
    const documentTypeOps = documentTypes
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()
    const docs = types
      ? documents.filter(d => types === d.documentTypeId)
      : documents
    const listItems = docs
      .map((v, k) => {
        const iconProps = {
          name: selected.includes(v.id) ? 'check circle' : 'circle outline',
          color: selected.includes(v.id) ? 'green' : 'grey',
          size: 'large'
        }
        return (
          <List.Item key={k} onClick={() => this.toggleSelection(v.id)}>
            <Grid verticalAlign='middle' columns='equal'>
              <Grid.Column>
                <Icon
                  size='large'
                  name={getFontAwesomeIconFromMIME(v.mimeType)}
                />
              </Grid.Column>
              <Grid.Column width={12}>
                <Header as='h4' sub color='blue'>
                  {documentTypes.getIn([`${v.documentTypeId}`, 'name'])}
                </Header>
                <Header as='h4'>
                  <Ellipsis
                    text={`${v.filename}.${v.extension}`}
                    basedOn='letters'
                    trimRight
                  />
                </Header>
              </Grid.Column>
              <Grid.Column floated='right'>
                <Icon {...iconProps} inverted />
              </Grid.Column>
            </Grid>
          </List.Item>
        )
      })
      .toList()
      .toJS()

    return (
      <div className='drawer-content'>
        <Form size='small'>
          <Form.Group>
            <Form.Dropdown
              width={12}
              name='types'
              placeholder='Choose a document type'
              options={documentTypeOps}
              value={types}
              search
              selection
              onChange={this.updateForm}
              selectOnBlur={false}
              clearable
            />
            <Form.Button
              fluid
              width={4}
              positive
              content='Add'
              disabled={!selected.length}
              onClick={() => onComplete(selected)}
            />
          </Form.Group>
        </Form>
        {fetching ? (
          <Loader style={{ marginTop: 32 }} active inline='centered' />
        ) : (
          <List verticalAlign='middle' selection animated relaxed size='small'>
            {listItems}
          </List>
        )}
      </div>
    )
  }
}

export default DocumentPicker
