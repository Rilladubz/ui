import React from 'react'
import { Button, Segment, Header, Icon } from 'semantic-ui-react'

const EmptyFileTypes = ({ add }) => (
  <Segment placeholder>
    <Header icon>
      <Icon color='blue' name='info circle' />
      This collection is empty.
    </Header>
    <Button primary onClick={() => add()} content='Add Documents' />
  </Segment>
)

export default EmptyFileTypes
