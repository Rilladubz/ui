import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/users'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/UsersListItem'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.users.getItems(state),
    params: state.users.get('params'),
    resourceName,
    resourceList,
    navView: 'container/settings',
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    loading: state.loading.effects.users.fetchAll || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    removeAction: id => dispatch.users.remove(id),
    refetch: () => dispatch.users.fetchAll(),
    prefetch: [() => dispatch.users.fetchAll()],
    changeParams: filters => dispatch.users.changeParams(filters),
    resetParams: () => dispatch.users.resetParams()
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
