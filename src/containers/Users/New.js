import { connect } from 'react-redux'

import { ResourceCreateView } from 'components/Views'
import { modelConfig } from 'models/users'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      roles: state.roles
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    navView: 'container/settings',
    loading: state.loading.effects.roles.fetchAll || false,
    saving: state.loading.effects.users.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveAction: user => dispatch.users.create(user),
    prefetch: [() => dispatch.roles.fetchAll()]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceCreateView)
