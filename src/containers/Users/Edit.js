import { connect } from 'react-redux'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/users'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.users.getOne(state, { id: match.params.id }),
    dependencies: {
      roles: state.roles
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    navView: 'container/settings',
    loading:
      state.loading.effects.roles.fetchAll || state.loading.effects.users.fetch,
    saving: state.loading.effects.users.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  const params = { with: 'roles' }
  return {
    saveAction: user => dispatch.users.update(user),
    prefetch: [
      () => dispatch.users.fetch({ id, params }),
      () => dispatch.roles.fetchAll()
    ]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceEditView)
