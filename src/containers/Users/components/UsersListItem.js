import React from 'react'
import { Header } from 'semantic-ui-react'
import Ellipsis from 'react-lines-ellipsis'

const UsersListItem = ({ item, index, dependencies }) => {
  return (
    <Header as='h5'>
      <Ellipsis
        text={`${item.firstName} ${item.lastName}`}
        basedOn='letters'
        trimRight
      />
      <Header.Subheader>{item.email}</Header.Subheader>
    </Header>
  )
}

export default UsersListItem
