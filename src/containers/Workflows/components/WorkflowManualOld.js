import React from 'react'
import {
  Input,
  Col,
  Row,
  Button,
  Switch,
  Timeline,
  Card,
  Empty,
  List,
  Avatar
} from 'antd'
import StepDetailForm from './StepDetailForm'
import { WorkflowBuilder } from 'components/AuthoringTool'

class WorkflowManual extends React.Component {
  render () {
    return <WorkflowBuilder />
  }
}

export default WorkflowManual
