import React, { Fragment } from 'react'
import { Grid, Header } from 'semantic-ui-react'
import Ellipsis from 'react-lines-ellipsis'

const WorkflowListItem = ({ item, index, dependencies }) => {
  return (
    <Fragment>
      <Grid verticalAlign='middle'>
        <Grid.Column textAlign='left' width={14}>
          <Header as='h4' sub color='blue'>
            {item.published ? 'Published' : 'Unpublished'}
          </Header>
          <Header as='h5'>
            <Ellipsis text={`${item.name}`} basedOn='letters' trimRight />
          </Header>
        </Grid.Column>
      </Grid>
    </Fragment>
  )
}

export default WorkflowListItem
