import React from 'react'
import PropTypes from 'prop-types'
import { Drawer, Select, Switch, Button, Icon } from 'antd'

import { ResourceCreateView } from 'components/Views'
import WorkflowEditor from 'components/WorkflowEditor'
import WorkflowManual from './WorkflowManualOld'
import { WorkflowBuilder } from 'components/AuthoringTool'

const Option = Select.Option

class WorkflowCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  state = {
    form: {
      documentTypeId: '',
      options: {
        startInstanceOnUpload: false
      }
    },
    workflowBuilderData: null,
    showManualWorkflowCreate: false
  }

  static contextTypes = {
    router: PropTypes.object
  }

  openDrawer = () =>
    this.setState({
      openManualDrawer: true
    })

  closeDrawer = () =>
    this.setState({
      openManualDrawer: false
    })

  updateForm = e => {
    const data = {}
    data['documentTypeId'] = e
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  updateOptions = e => {
    const { options } = this.state.form
    options['startInstanceOnUpload'] = !options['startInstanceOnUpload']
    this.setState({
      form: {
        ...this.state.form,
        ...{ options }
      }
    })
  }

  onSave = ({ xml, defs }) => {
    const { name, id } = defs.rootElements[0]
    const { form } = this.state
    const { saveAction, resourceName } = this.props
    const newWorkflow = {
      ...form,
      bpmnXml: xml,
      bpmnId: id,
      name: name || id
    }
    saveAction(newWorkflow).then(data => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  openDrawer = () => {
    this.setState({
      isDrawerOpen: true
    })
  }

  closeDrawer = () => {
    this.setState({
      isDrawerOpen: false,
      showManualWorkflowCreate: false
    })
  }

  handleManualWorkflow = () => {
    this.setState({ showManualWorkflowCreate: true })
  }

  backToWorkflowSettings = () => {
    this.setState({ showManualWorkflowCreate: false })
  }

  switchDrawerContent = () => {
    const { dependencies } = this.props
    const { form } = this.state
    const documentTypeOps = dependencies.documentTypes
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()
    return (
      <div>
        <div style={{ width: '100%' }}>
          <label style={{ fontWeight: 'bold', color: 'primary' }}>
            Start a document type
          </label>
          <Select
            showSearch
            style={{ width: '100%', marginBottom: 30 }}
            value={form.documentTypeId}
            placeholder='Choose a document type'
            optionFilterProp='children'
            onChange={this.updateForm}
            filterOption={(input, option) =>
              option.props.children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {documentTypeOps.map((v, k) => (
              <Option value={v.value} key={k}>
                {v.text}
              </Option>
            ))}
          </Select>
        </div>
        <Switch
          style={{ width: '100%', marginBottom: 30 }}
          unCheckedChildren='Start new instance on creation'
          checked={form.options.startInstanceOnUpload}
          checkedChildren='Start new instance on creation'
          onChange={this.updateOptions}
        />
      </div>
    )
  }

  generateFromBuilder = workflow => {
    const { match } = this.props

    this.setState(
      {
        workflowBuilderData: workflow
      },
      this.context.router.history.push(`${match.path}?mode='advanced'`)
    )
  }

  renderContent = () => {
    const { isDrawerOpen, form, workflowBuilderData } = this.state
    const {
      resourceName,
      resourceForm,
      saving,
      dependencies,
      mode
    } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]

    const isValid = form.documentTypeId

    return (
      <div className='workflow-container'>
        <Drawer
          onClose={this.closeDrawer}
          visible={isDrawerOpen}
          title={<div>Workflow Settings</div>}
          closable
          width={320}
        >
          {this.switchDrawerContent()}
        </Drawer>
        {mode === 'simple' ? (
          <WorkflowBuilder
            {...dependencies}
            onSave={this.generateFromBuilder}
          />
        ) : (
          <WorkflowEditor
            workflowBuilderData={workflowBuilderData}
            onSave={isValid ? this.onSave : null}
            openDrawer={this.openDrawer}
          />
        )}
      </div>
    )
  }
}

export default WorkflowCreateView
