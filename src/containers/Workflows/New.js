import { connect } from 'react-redux'

import _ from 'lodash'

import WorkflowCreateView from './components/WorkflowCreateView'
import { modelConfig } from 'models/workflows'
import store from 'store'

const mapStateToProps = (state, { match, location }) => {
  const { resourceName, resourceForm } = modelConfig
  let mode = 'simple'
  if (location.search) {
    const query = new URLSearchParams(location.search)
    mode = query.get('mode')
  }
  return {
    dependencies: {
      documentTypes: store.select.documentTypes.getItems(state),
      recordTypes: store.select.recordTypes.getItems(state),
      users: store.select.users.getItems(state),
      roles: store.select.roles.getItems(state)
    },
    mode,
    resourceName,
    resourceForm,
    slideoutWidth: 1280,
    loading:
      state.loading.models.workflows ||
      state.loading.models.documentTypes ||
      state.loading.models.recordsTypes ||
      state.loading.models.roles ||
      state.loading.models.users ||
      false,
    saving:
      state.loading.effects.workflows.update ||
      state.loading.effects.workflows.unpublish ||
      state.loading.effects.workflows.publish ||
      state.loading.effects.workflows.duplicate ||
      false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  return {
    prefetch: [
      () => dispatch.documentTypes.fetchAll(),
      () => dispatch.roles.fetchAll(),
      () => dispatch.users.fetchAll(),
      () => dispatch.recordTypes.fetchAll()
    ],
    saveAction: workflow => dispatch.workflows.create(workflow)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkflowCreateView)
