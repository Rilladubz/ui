import { connect } from 'react-redux'

import WorkflowEditView from './components/WorkflowEditView'
import { modelConfig } from 'models/workflows'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName } = modelConfig
  return {
    item: store.select.workflows.getOne(state, { id: match.params.id }),
    dependencies: {
      documentTypes: state.documentTypes.get('list')
    },
    resourceName,
    slideoutWidth: 1280,
    navView: 'container/settings',
    loading:
      state.loading.effects.documentTypes.fetchAll ||
      state.loading.effects.workflows.fetch ||
      false,
    saving:
      state.loading.effects.workflows.update ||
      state.loading.effects.workflows.unpublish ||
      state.loading.effects.workflows.publish ||
      state.loading.effects.workflows.duplicate ||
      false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    prefetch: [
      () => dispatch.workflows.fetch({ id }),
      () => dispatch.documentTypes.fetchAll()
    ],
    saveAction: workflow => dispatch.workflows.update(workflow),
    publish: id => dispatch.workflows.publish(id),
    unpublish: id => dispatch.workflows.unpublish(id),
    duplicate: id => dispatch.workflows.duplicate(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkflowEditView)
