import { connect } from 'react-redux'
import LoginView from './components/LoginView'

const mapStateToProps = (state, ownProps) => {
  return {
    message: state.accountRecovery.message,
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    doLogin: payload => dispatch.auth.login(payload)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginView)
