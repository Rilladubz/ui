import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Icon, Input, Button, Divider } from 'antd'

class LoginView extends Component {
  handleLogin = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.doLogin(values)
      }
    })
  }

  render () {
    const { getFieldDecorator } = this.props.form
    return (
      <div className='ui six wide column'>
        <div className='ui very padded segment'>
          <h1 className='ui teal image header'>
            <div className='content'>Brands ERP</div>
          </h1>
          <Form onSubmit={this.handleLogin}>
            <Form.Item>
              {getFieldDecorator('email', {
                rules: [{ required: true, message: 'Please input your email!' }]
              })(
                <Input
                  size='large'
                  prefix={
                    <Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Email'
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [
                  { required: true, message: 'Please input your Password!' }
                ]
              })(
                <Input
                  size='large'
                  prefix={
                    <Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  type='password'
                  placeholder='Password'
                />
              )}
            </Form.Item>
            <Form.Item>
              <Button
                type='primary'
                style={{
                  width: '100%',
                  backgroundColor: '#00b5ad',
                  borderColor: 'transparent'
                }}
                htmlType='submit'
                className='login-form-button'
              >
                Login
              </Button>
            </Form.Item>
            <Form.Item>
              <a className='login-form-forgot' href='forgot'>
                Forgot password?
              </a>
            </Form.Item>
          </Form>
          <Divider type='horizontal'>Or</Divider>
          <Form.Item>
            <Button type='primary' href='signup' style={{ width: '100%' }}>
              Sign Up
            </Button>
          </Form.Item>
        </div>
      </div>
    )
  }
}

LoginView.propTypes = {
  doLogin: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(LoginView)

export default WrappedNormalLoginForm
