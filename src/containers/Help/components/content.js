import React, { Component } from 'react'
import { Layout } from 'antd'

const { Footer, Content } = Layout

class HelpContent extends Component {
  render () {
    const { contentData, back } = this.props
    return (
      <Layout style={{ backgroundColor: 'transparent', height: '100%' }}>
        <Content>{contentData()}</Content>
        <Footer style={{ backgroundColor: 'transparent' }}>
          <a style={{ float: 'right' }} onClick={back}>
            Back
          </a>
        </Footer>
      </Layout>
    )
  }
}

export default HelpContent
