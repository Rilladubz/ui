import React, { Component } from 'react'
import HelpContent from './content'
import { Collapse } from 'antd'

const Panel = Collapse.Panel

class TopicContent extends Component {
  renderContent = () => {
    const { topic, goBack } = this.props
    switch (topic) {
      case 'Tasks':
        return <p>This is where you could access tasks assigned</p>
      case 'Documents':
        return <p>This is where you could access documents</p>
      case 'Records':
        return <p>This is where you could access records</p>
      case 'Collections':
        return <p>This is where you could access collections</p>
      case 'Settings':
        return (
          <Collapse bordered={false}>
            <Panel header='ACCESS & AUTHORIZATION' key='1'>
              <p>
                This is where you could manage users, edit roles and premissions
              </p>
            </Panel>
            <Panel header='DOCUMENTS & DATA' key='2'>
              <p>
                This is where you could see pending files without a file type
                yet, you could assign a file type by clicking on the file.
              </p>
              <p>
                You could create a document type like 'gif' and assign it to a
                document later.
              </p>
              <p>
                You could also create new record types here, and assign it to a
                record lator
              </p>
            </Panel>
            <Panel header='AUTOMATION' key='3'>
              <p>
                This is where you could create a new work flow by using the work
                flow editor.
              </p>
            </Panel>
          </Collapse>
        )
      default:
    }
  }
  render () {
    return (
      <HelpContent contentData={this.renderContent} back={this.props.goBack} />
    )
  }
}

export default TopicContent
