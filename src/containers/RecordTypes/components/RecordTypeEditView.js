import React from 'react'
import PropTypes from 'prop-types'
import { Header } from 'semantic-ui-react'
import {
  Button,
  Form,
  Input,
  InputNumber,
  Row,
  Col,
  Layout,
  Card,
  Select,
  Menu,
  Empty,
  Switch
} from 'antd'
import _ from 'lodash'
import shortid from 'shortid'
import { fromJS, Record } from 'immutable'

import { ResourceEditView } from 'components/Views'
import {
  Form as JSONForm,
  ObjectFieldBuilderTemplate,
  BuilderFieldTemplate
} from 'components/JSONschema'
import DetailsForm from './DetailsForm'
import FormBuilder from 'components/FormBuilder'

shortid.characters(
  '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$-'
)

const InitData = Record({
  schema: fromJS({
    type: 'object',
    required: [],
    properties: {}
  }),
  uiSchema: fromJS({
    'ui:order': []
  }),
  meta: fromJS({
    listFields: fromJS([])
  }),
  selectedField: null
})

class RecordTypeEditView extends ResourceEditView {
  constructor (props) {
    super(props)
    const InitData = Record({
      schema: fromJS(props.item ? JSON.parse(props.item.schema) : {}),
      uiSchema: fromJS(props.item ? JSON.parse(props.item.uiSchema) : {}),
      meta: fromJS(props.item ? JSON.parse(props.item.meta) : {}),
      selectedField: null
    })
    this.state = {
      toggleView: 'fields',
      form: {
        name: {
          value: props.item ? props.item.name : ''
        },
        description: {
          value: props.item ? props.item.description : ''
        }
      },
      data: new InitData()
    }
  }

  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  setImmState = fn => {
    this.setState(({ data }) => ({
      data: fn(data)
    }))
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.item !== nextProps.item) {
      const InitData = Record({
        schema: fromJS(JSON.parse(nextProps.item.schema)),
        uiSchema: fromJS(JSON.parse(nextProps.item.uiSchema)),
        meta: fromJS(JSON.parse(nextProps.item.meta)),
        selectedField: null
      })
      this.setState({
        form: {
          id: nextProps.item.id,
          name: {
            value: nextProps.item.name
          },
          description: {
            value: nextProps.item.description
          }
        },
        data: new InitData()
      })
    }
  }

  componentWillMount () {
    const { prefetch } = this.props
    if (prefetch && prefetch.length) {
      prefetch.forEach(p => p())
    }
  }

  updateView = key => this.setState({ toggleView: key })

  handleFormChange = changedFields => {
    this.setState(({ form }) => ({
      form: { ...form, ...changedFields }
    }))
  }

  handleSave = () => {
    const { saveAction, resourceName, item } = this.props
    const { form, data } = this.state
    const { schema, meta, uiSchema } = data.toJS()
    const newRecordType = {
      id: item.id,
      ..._.mapValues(form, v => v.value),
      schema: JSON.stringify(schema),
      uiSchema: JSON.stringify(uiSchema),
      meta: JSON.stringify(meta)
    }
    saveAction(newRecordType).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderContent = () => {
    const { data, toggleView, form } = this.state
    const { resourceName, resourceForm, saving } = this.props
    const tabList = [
      { key: 'fields', tab: 'Fields' },
      { key: 'options', tab: 'Options' }
    ]

    return (
      <Layout.Content>
        <Header as='h2' dividing>
          Edit Record Type - {form.id}
        </Header>
        <Row>
          <Col>
            <Card
              title='Record Details'
              size='small'
              style={{ marginBottom: '1.5rem' }}
            >
              <DetailsForm {...form} onChange={this.handleFormChange} />
            </Card>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Card
              tabList={tabList}
              onTabChange={k => this.updateView(k)}
              title='Form Builder'
              size='small'
              activeTabKey={toggleView}
            >
              <FormBuilder
                data={data}
                setImmState={this.setImmState}
                view={toggleView}
              />
            </Card>
          </Col>
        </Row>
        <Row justify='end' type='flex'>
          <Col span={2} style={{ paddingTop: '2em' }}>
            <Button block type='primary' onClick={() => this.handleSave()}>
              Save
            </Button>
          </Col>
        </Row>
      </Layout.Content>
    )
  }
}

export default RecordTypeEditView
