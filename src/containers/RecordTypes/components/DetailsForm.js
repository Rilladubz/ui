import React from 'react'
import { Form, Input, Row, Col } from 'antd'

const DetailsForm = Form.create({
  name: 'form',
  onFieldsChange (props, changedFields) {
    props.onChange(changedFields)
  },
  mapPropsToFields (props) {
    return {
      name: Form.createFormField({
        ...props.name,
        value: props.name.value
      }),
      description: Form.createFormField({
        ...props.description,
        value: props.description.value
      })
    }
  }
})(props => {
  const { getFieldDecorator } = props.form
  return (
    <Form layout='vertical' style={{ padding: '0.5rem 1rem 1rem 1rem' }}>
      <Row gutter={24}>
        <Col span={8}>
          <Form.Item
            label='Record Name'
            style={{ paddingBottom: 0, marginBottom: 0 }}
          >
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Record name is required.' }]
            })(<Input />)}
          </Form.Item>
        </Col>
        <Col span={16}>
          <Form.Item
            label='Record Description'
            style={{ paddingBottom: 0, marginBottom: 0 }}
          >
            {getFieldDecorator('description')(<Input />)}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
})

export default DetailsForm
