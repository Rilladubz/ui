import React from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Button,
  Header,
  Segment,
  Grid,
  Label,
  Menu
} from 'semantic-ui-react'
import _ from 'lodash'

import { ResourceCreateView } from 'components/Views'
import { toJson, shouldRender } from 'utils/render'
import { Form as JSONForm } from 'components/JSONschema'
import Editor from 'components/Editor'

class RecordTypeCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    toggleView: 'json',
    form: {
      name: '',
      description: ''
    },
    schema: {
      type: 'object',
      required: [],
      properties: {
        myField: { type: 'string', title: 'My Field' }
      }
    },
    formData: { myField: '' },
    uiSchema: {
      myField: {
        'ui:emptyValue': ''
      }
    },
    meta: {
      listFields: []
    }
  }

  componentDidMount () {
    const { navigationViewController } = this.props
  }

  shouldComponentUpdate (nextProps, nextState) {
    return shouldRender(this, nextProps, nextState)
  }

  updateSchema = schema => {
    this.setState({
      schema
    })
  }

  updateUiSchema = uiSchema => {
    this.setState({
      uiSchema
    })
  }

  updateFormData = ({ formData }) => {
    this.setState({
      formData
    })
  }

  updateMeta = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      meta: {
        ...this.state.meta,
        ...data
      }
    })
  }

  updateView = (e, { name }) => this.setState({ toggleView: name })

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { form, schema, uiSchema, meta } = this.state
    const newRecordType = {
      ...form,
      schema,
      uiSchema,
      meta
    }
    saveAction(newRecordType).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderPane = () => {
    const { toggleView } = this.state
    return toggleView === 'options'
      ? this.renderOptions()
      : this.renderSchemaBuilder()
  }

  renderOptions = () => {
    const { schema, meta } = this.state
    const listFieldOps = _.keys(schema.properties).map((v, k) => ({
      key: k,
      text: v,
      value: v
    }))
    return (
      <Form size='small'>
        <Form.Dropdown
          multiple
          selection
          options={listFieldOps}
          name='listFields'
          label='List Display Fields'
          value={meta.listFields}
          onChange={this.updateMeta}
        />
      </Form>
    )
  }

  renderSchemaBuilder = () => {
    const { schema, formData, uiSchema, toggleView } = this.state

    const viewerProps = {
      formData,
      onChange: this.updateFormData,
      schema,
      uiSchema,
      className: 'ui tiny form'
    }

    const editorProps = {
      onChange: toggleView === 'ui' ? this.updateUiSchema : this.updateSchema,
      code: toggleView === 'ui' ? toJson(uiSchema) : toJson(schema),
      label: toggleView
    }

    return (
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column width={9}>
            <Editor {...editorProps} />
          </Grid.Column>
          <Grid.Column width={7}>
            <Segment size='small'>
              <Label attached='top left' content='Form Preview' />
              <JSONForm {...viewerProps} />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  renderContent = () => {
    const { uiState, schema, toggleView } = this.state
    const { name, description } = this.state.form
    const { resourceName, resourceForm, saving } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const isValid = name !== ''
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Create Record Type</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment loading={saving}>
              <Form size='small' onSubmit={this.handleSave}>
                <Form.Group widths='equal'>
                  <Form.Input
                    label='Name'
                    type='text'
                    name='name'
                    value={name}
                    required
                    onChange={this.updateForm}
                  />
                  <Form.Input
                    label='Description'
                    type='text'
                    name='description'
                    value={description}
                    onChange={this.updateForm}
                  />
                </Form.Group>
              </Form>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Menu size='tiny' attached='top'>
              <Menu.Item
                name='json'
                content='JSON Schema'
                active={toggleView === 'json'}
                onClick={this.updateView}
              />
              <Menu.Item
                name='ui'
                content='UI Schema'
                active={toggleView === 'ui'}
                onClick={this.updateView}
              />
              <Menu.Item
                name='options'
                content='Options'
                active={toggleView === 'options'}
                onClick={this.updateView}
              />
            </Menu>
            <Segment attached='bottom'>{this.renderPane()}</Segment>
            <Button
              floated='right'
              disabled={!isValid}
              positive
              content='Save'
              onClick={() => this.handleSave()}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default RecordTypeCreateView
