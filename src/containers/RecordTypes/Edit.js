import { connect } from 'react-redux'
import store from 'store'

import RecordTypeEditView from './components/RecordTypeEditView'
import { modelConfig } from 'models/recordTypes'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.recordTypes.getOne(state, { id: match.params.id }),
    resourceName,
    resourceForm,
    loading: state.loading.effects.recordTypes.fetch || false,
    saving: state.loading.effects.recordTypes.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: recordType => dispatch.recordTypes.update(recordType),
    prefetch: [() => dispatch.recordTypes.fetch({ id })]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordTypeEditView)
