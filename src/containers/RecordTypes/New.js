import { connect } from 'react-redux'

import RecordTypeCreateView from './components/RecordTypeCreateView'
import { modelConfig } from 'models/recordTypes'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    resourceName,
    resourceForm,
    saving: state.loading.effects.recordTypes.create || false,
    slideoutWidth: '90%'
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [],
    saveAction: recordType => dispatch.recordTypes.create(recordType)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordTypeCreateView)
