export { default as ListRecordTypes } from './List'
export { default as NewRecordType } from './New'
export { default as EditRecordType } from './Edit'
