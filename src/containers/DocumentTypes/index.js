export { default as ListDocTypes } from './List'
export { default as EditDocType } from './Edit'
export { default as NewDocType } from './New'
