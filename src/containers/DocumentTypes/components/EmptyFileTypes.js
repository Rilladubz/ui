import React from 'react'
import { Card, Avatar } from 'antd'
const { Meta } = Card

const EmptyFileTypes = () => (
  <Card>
    <Meta
      avatar={<Avatar icon='exclamation' />}
      description='No file types selected. Try adding from the right menu.'
    />
  </Card>
)

export default EmptyFileTypes
