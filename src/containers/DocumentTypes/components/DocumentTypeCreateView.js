import React from 'react'
import PropTypes from 'prop-types'
import WrappedHorizontalForm from '../Form'
import { Tabs, Row, Col, Divider, Button, Card, Avatar, Tag } from 'antd'
import { ResourceCreateView } from 'components/Views'
import EmptyFileTypes from './EmptyFileTypes'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const TabPane = Tabs.TabPane
const { Meta } = Card

class DocumentTypeCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    form: {
      name: '',
      description: '',
      properties: {
        fileTypes: []
      }
    },
    activeMenuItem: 'image'
  }

  componentDidMount () {
    const { navigationViewController } = this.props
  }

  updateForm = (e, name) => {
    const data = {}
    data[name] = e.target.value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  addFileType = item => {
    this.setState({
      form: {
        ...this.state.form,
        name: item.name,
        description: item.filenames.join(','),
        properties: {
          ...this.state.form.properties,
          fileTypes: [...this.state.form.properties.fileTypes, item.id]
        }
      }
    })
  }

  removeFileType = id => {
    const { form } = this.state
    form.properties.fileTypes.splice(id, 1)
    this.setState({
      form: {
        ...this.state.form,
        ...form
      }
    })
  }

  menuClick = e => this.setState({ activeMenuItem: e })

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { form } = this.state
    const newCollection = {
      ...form
    }
    saveAction(newCollection).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderContent = () => {
    const {
      activeMenuItem,
      form: { name, description, properties }
    } = this.state

    const { saving, dependencies } = this.props

    const isValid =
      name !== '' && description !== '' && properties.fileTypes.length > 0

    const listItems = dependencies.fileTypes[activeMenuItem].filter(
      t => !properties.fileTypes.includes(t.id)
    )

    const fileTypeLabels = properties.fileTypes.map((v, k) => (
      <Tag closable onClose={() => this.removeFileType(k)}>
        {dependencies.fileTypes.all.find(f => f.id === v)['name']}
      </Tag>
    ))

    const menuItems = [
      {
        key: 'image',
        active: activeMenuItem === 'image',
        name: 'image'
      },
      {
        key: 'video',
        active: activeMenuItem === 'video',
        name: 'video'
      },
      {
        key: 'audio',
        active: activeMenuItem === 'audio',
        name: 'audio'
      },
      {
        key: 'text',
        active: activeMenuItem === 'text',
        name: 'text'
      },
      {
        key: 'application',
        active: activeMenuItem === 'application',
        name: 'application'
      }
    ]
    return (
      <div>
        <h1>Create Document Type</h1>
        {!saving && (
          <WrappedHorizontalForm
            updateForm={this.updateForm}
            value={{ name, description }}
          />
        )}
        <Card style={{ border: '1px solid #F65E7D' }}>
          <Meta
            style={{ display: 'flex', alignItems: 'center' }}
            avatar={<Avatar icon='question' />}
            description='Choose file types from the lists below that this document type should allow'
          />
        </Card>
        {!saving && (
          <div style={{ marginTop: 20 }}>
            <Row gutter={24}>
              <Col span={12}>
                <h3>Selected File Types</h3>
                {fileTypeLabels.length ? (
                  <div>{fileTypeLabels}</div>
                ) : (
                  <EmptyFileTypes />
                )}
              </Col>
              <Col span={12}>
                <h3>Available File Types</h3>
                <Tabs
                  onTabClick={this.menuClick}
                  defaultActiveKey={this.state.activeMenuItem}
                >
                  {menuItems.map((r, k) => {
                    return (
                      <TabPane tab={r.name} key={r.key}>
                        {listItems.map((r, k) => {
                          return (
                            <div
                              key={k}
                              onClick={() => {
                                this.addFileType(r)
                              }}
                              style={{ cursor: 'pointer' }}
                            >
                              <Row>
                                <Col span={3}>
                                  <FontAwesomeIcon
                                    className='anticon'
                                    icon='file'
                                  />
                                </Col>
                                <Col span={9}>
                                  <div>{r.name}</div>
                                  <div>{r.filenames.join(',')}</div>
                                </Col>
                              </Row>
                              <Divider type='horizontal' />
                            </div>
                          )
                        })}
                      </TabPane>
                    )
                  })}
                </Tabs>
              </Col>
            </Row>
          </div>
        )}
        <Button
          type='primary'
          disabled={!isValid}
          style={{ float: 'right', marginBottom: 10 }}
          onClick={() => this.handleSave()}
        >
          Save
        </Button>
      </div>
    )
  }
}

export default DocumentTypeCreateView
