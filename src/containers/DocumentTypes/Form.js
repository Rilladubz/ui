import React from 'react'
import PropTypes from 'prop-types'

import { Form, Input, Row, Col } from 'antd'

class HorizontalDocumentForm extends React.Component {
  static propTypes = {
    value: PropTypes.object,
    updateForm: PropTypes.func
  }
  render () {
    return (
      <Form layout='vertical'>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label='Name'>
              <Input
                placeholder='Name of document type'
                onChange={e => this.props.updateForm(e, 'name')}
                value={this.props.value.name}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label='Description'>
              <Input
                placeholder='Description of document'
                onChange={e => this.props.updateForm(e, 'description')}
                value={this.props.value.description}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    )
  }
}

export default HorizontalDocumentForm
