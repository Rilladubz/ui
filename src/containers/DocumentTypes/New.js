import { connect } from 'react-redux'

import * as fileTypes from 'utils/fileTypes'

import DocumentTypeCreateView from './components/DocumentTypeCreateView'
import { modelConfig } from 'models/documentTypes'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      fileTypes
    },
    resourceName,
    resourceForm,
    navView: 'container/settings',
    saving: state.loading.effects.documentTypes.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [],
    saveAction: type => dispatch.documentTypes.create(type)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentTypeCreateView)
