import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/documentTypes'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/DocumentTypesListItem'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.documentTypes.getItems(state),
    params: state.documentTypes.get('params'),
    resourceName,
    resourceList,
    navView: 'container/settings',
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    loading: state.loading.effects.documentTypes.fetchAll || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.documentTypes.fetchAll()],
    refetch: () => dispatch.documentTypes.fetchAll(),
    changeParams: filters => dispatch.documentTypes.changeParams(filters),
    resetParams: () => dispatch.documentTypes.resetParams(),
    removeAction: id => dispatch.documentTypes.remove(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
