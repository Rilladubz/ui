import { connect } from 'react-redux'

import * as fileTypes from 'utils/fileTypes'

import DocumentTypeEditView from './components/DocumentTypeEditView'
import { modelConfig } from 'models/documentTypes'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.documentTypes.getOne(state, { id: match.params.id }),
    dependencies: {
      fileTypes
    },
    resourceName,
    resourceForm,
    navView: 'container/settings',
    saving: state.loading.effects.documentTypes.update || false,
    loading: state.loading.effects.documentTypes.fetch || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: type => dispatch.documentTypes.update(type),
    prefetch: [() => dispatch.documentTypes.fetch({ id })]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentTypeEditView)
