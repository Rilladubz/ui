import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/tasks'
import EditContainer from './Complete'
import itemRenderer from './components/MyTaskListItem'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: store.select.tasks.getMyTasks(state),
    params: store.select.tasks.getParams(state),
    dependencies: {
      authUser: store.select.auth.getAuthUser(state)
    },
    itemRenderer,
    detailView: EditContainer,
    loading: state.loading.models.tasks || false,
    navView: 'container/tasks',
    resourceList,
    resourceName: {
      singular: resourceName.singular,
      plural: `My ${resourceName.plural}`,
      route: `/my-tasks`
    }
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  const state = store.getState()
  const params = { assignee: `${state.auth.user.id}` }
  return {
    prefetch: [
      () => dispatch.users.fetchAll(),
      () => dispatch.tasks.fetchAll(params)
    ],
    refetch: () => dispatch.tasks.fetchAll(params),
    changeParams: filters => dispatch.tasks.changeParams(filters),
    claimTask: id => dispatch.tasks.claimTask(id),
    resetParams: () => dispatch.tasks.resetParams()
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
