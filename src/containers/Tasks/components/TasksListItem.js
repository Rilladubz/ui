import React from 'react'
import { Header } from 'semantic-ui-react'
import Ellipsis from 'react-lines-ellipsis'

const TasksListItem = ({ item, index, dependencies }) => {
  const { users } = dependencies
  let assignee = 'Unassigned'
  if (item.assignee) {
    const id = parseInt(item.assignee)
    const user = users[id]
    assignee = `${user.firstName} ${user.lastName}`
  }
  return (
    <>
      <Header as='h5'>
        <Ellipsis text={item.name} basedOn='letters' trimRight />
      </Header>
      {assignee}
    </>
  )
}

export default TasksListItem
