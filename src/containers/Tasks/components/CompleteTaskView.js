import React from 'react'
import PropTypes from 'prop-types'
import { Header, Segment, Grid, Embed, Button, Modal } from 'semantic-ui-react'

import { ResourceEditView } from 'components/Views'
import { ContainerLoader } from 'components/Loader'
import ResourceNotFound from 'components/Views/ResourceNotFound'
import { shouldRender } from 'utils/render'
import { Form as JSONForm } from 'components/JSONschema'
import ErrorList from 'components/JSONschema/ErrorList'

class CompleteTaskView extends ResourceEditView {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.state = {
      data: {},
      dependentRecord: null
    }
  }

  componentDidMount () {
    const { prefetch, resourceName, match } = this.props
    if (prefetch.length && match.path === `/tasks/:id/complete`) {
      prefetch.forEach(p => p())
    }
  }

  componentWillUpdate (nextProps, nextState) {
    const { item } = nextProps
    const oldItem = this.props.item
    if (!oldItem && item) {
      const formVars = nextProps.item.getIn(['formVariables'])
      if (formVars.formType.value === 'record') {
        const { recordTypes } = nextProps.dependencies
        const record = recordTypes.find(r => r.id === formVars.recordType.value)
        if (record) {
          this.setState({ dependentRecord: record })
        } else {
          this.props.fetchDep(formVars.recordType.value)
        }
      } else {
        const data = {}
        Object.entries(formVars).forEach((v, k) => {
          if (v[1].type === 'boolean') {
            data[v[0]] = false
          } else {
            data[v[0]] = ''
          }
        })
        this.setState({
          data
        })
      }
    }

    if (
      nextProps.dependencies.recordTypes !== this.props.dependencies.recordTypes
    ) {
      const formVars = nextProps.item.getIn(['formVariables'])
      if (formVars.formType.value === 'record') {
        const { recordTypes } = nextProps.dependencies
        const record = recordTypes.get(parseInt(formVars.recordType.value))
        if (record) {
          this.setState({ dependentRecord: record.toJS() })
        } else {
          this.props.fetchDep(formVars.recordType.value)
        }
      }
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return shouldRender(this, nextProps, nextState)
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      ...data
    })
  }

  handleSave = ({ formData }) => {
    const { saveAction, resourceName, item } = this.props
    const taskForm = {
      variables: {
        data: { value: JSON.stringify(formData), type: 'String' }
      }
    }
    saveAction(item.id, taskForm).then(() => {
      this.context.router.history.push('/tasks?default=self')
    })
  }

  renderSchemaBuilder = () => {
    const { formVariables } = this.props.item
    const { saving } = this.props
    const { dependentRecord } = this.state
    const viewerProps = {
      formData: this.state.data,
      schema:
        formVariables.formType.value === 'record'
          ? JSON.parse(dependentRecord.schema)
          : formVariables.schema,
      uiSchema:
        formVariables.formType.value === 'record'
          ? JSON.parse(dependentRecord.uiSchema)
          : formVariables.uiSchema,
      liveValidate: false,
      onSubmit: this.handleSave,
      ErrorList,
      className: 'ui tiny form'
    }

    return <JSONForm {...viewerProps} />
  }

  renderContent = () => {
    const { saving, dependencies, item } = this.props
    const { dependentRecord } = this.state
    const formVars = item.get('formVariables')
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Task Form - {dependencies.task.name}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Modal
              closeOnDimmerClick={false}
              closeIcon
              trigger={
                <Button
                  icon='external alternate'
                  content='Review Document'
                  attached='top'
                />
              }
            >
              <Modal.Header>Review Document</Modal.Header>
              <Modal.Content>
                <Embed active url={formVars.media_link.value} />
              </Modal.Content>
            </Modal>
            <Segment attached='bottom' loading={saving}>
              {formVars.formType.value === 'record' && dependentRecord
                ? this.renderSchemaBuilder()
                : null}
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  render () {
    const {
      loading,
      saving,
      item,
      dependencies: { task }
    } = this.props
    if (loading && !saving) {
      return <ContainerLoader />
    } else {
      return item && task ? this.renderContent() : <ResourceNotFound />
    }
  }
}

export default CompleteTaskView
