import { connect } from 'react-redux'

import CompleteTaskView from './components/CompleteTaskView'
import { modelConfig } from 'models/tasks'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName } = modelConfig
  return {
    item: store.select.taskVariables.getOne(state, { id: match.params.id }),
    dependencies: {
      task: store.select.tasks.getOne(state, { id: match.params.id }),
      recordTypes: store.select.recordTypes.getList(state)
    },
    loading:
      state.loading.effects.tasks.fetch ||
      state.loading.effects.taskVariables.fetch ||
      state.loading.effects.recordTypes.fetch,
    resourceName: {
      singular: resourceName.singular,
      plural: `Tasks`,
      route: '/tasks/:id/complete'
    }
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  const params = {
    variableNames: 'schema,uiSchema,media_link,media_id,formType,recordType'
  }
  return {
    prefetch: [() => dispatch.taskVariables.fetch({ id, params })],
    fetchDep: recordTypeId => dispatch.recordTypes.fetch({ id: recordTypeId }),
    saveAction: (taskId, data) => dispatch.tasks.complete({ taskId, data })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompleteTaskView)
