import { connect } from 'react-redux'

import RecordEditView from './components/RecordEditView'
import { modelConfig } from 'models/records'
import store from 'store'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: store.select.records.getOne(state, { id: match.params.id }),
    dependencies: {
      recordTypes: state.recordTypes.get('list')
    },
    resourceName,
    resourceForm,
    loading:
      state.loading.effects.records.fetch ||
      state.loading.effects.recordTypes.fetchAll ||
      false,
    saving: state.loading.effects.records.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: record => dispatch.records.update(record),
    prefetch: [
      () => dispatch.records.fetch({ id }),
      () => dispatch.recordTypes.fetchAll()
    ]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordEditView)
