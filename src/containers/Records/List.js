import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/records'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/RecordsListItem'
import store from 'store'

const mapStateToProps = (state, { location, match }) => {
  const { resourceName, resourceList } = modelConfig
  const authUser = store.select.auth.getAuthUser(state)
  const viewId = new URLSearchParams(location.search).get('view')
  const defaultView = new URLSearchParams(location.search).get('default')
  const view = viewId ? store.select.views.getOne(state, { id: viewId }) : null

  return {
    items: store.select.records.getItems(state),
    params: state.records.get('params'),
    dependencies: {
      recordTypes: store.select.recordTypes.getList(state)
    },
    authUser,
    resourceName,
    resourceList,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    customViews: true,
    viewType: 'records',
    defaultView,
    view,
    viewId,
    loading:
      state.loading.effects.records.fetchAll ||
      state.loading.effects.recordTypes.fetchAll ||
      state.loading.effects.views.fetchByType
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [
      () => dispatch.recordTypes.fetchAll(),
      () => dispatch.views.fetchByType('records')
    ],
    removeAction: id => dispatch.records.remove(id),
    refetch: () => dispatch.records.fetchAll(),
    changeParams: filters => dispatch.records.changeParams(filters),
    resetParams: () => dispatch.records.resetParams(),
    saveView: view => dispatch.views.create(view),
    renameView: view => dispatch.views.rename(view),
    deleteView: id => dispatch.views.remove(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterListView)
