import { connect } from 'react-redux'

import RecordCreateView from './components/RecordCreateView'
import { modelConfig } from 'models/records'
import store from 'store'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      recordTypes: store.select.recordTypes.getList(state)
    },
    resourceName,
    resourceForm,
    navView: 'container/records',
    loading: state.loading.effects.recordTypes.fetchAll || false,
    saving: state.loading.effects.records.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveAction: record => dispatch.records.create(record),
    prefetch: [() => dispatch.recordTypes.fetchAll()]
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordCreateView)
