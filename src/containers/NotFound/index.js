import { connect } from 'react-redux'
import NotFoundView from './components/NotFoundView'

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotFoundView)
