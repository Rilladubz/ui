import React, { Component } from 'react'
// import { Grid, Header, Form } from 'semantic-ui-react';

class NotFoundView extends Component {
  render = () => {
    return (<div className='column'>
      <h1 className='ui teal image header'>
        <div className='content'>Page not found</div>
      </h1>
      <p>We don't know how you ended up here, but let's get you <a href='/'>back to the dashboard</a>.</p>
    </div>)
  }
}

export default NotFoundView
