import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { Redirect, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import ReduxToastr from 'react-redux-toastr'
import { Sidebar } from 'components/Navigation'
import { Layout } from 'antd'

import store from 'store'
import { AppLoader } from 'components/Loader'
import { resourceRoutes, adminRoutes } from 'routes'

class App extends Component {
  static contextTypes = {
    router: PropTypes.object
  }

  goTo = location => {
    this.context.router.history.push(location)
  }

  render () {
    const { children, user, isLoading, path, logoutUser } = this.props
    return (
      <Layout style={{ height: '100vh' }}>
        {isLoading ? <AppLoader /> : null}
        <Layout.Sider
          id='global-sidebar'
          collapsible
          collapsed
          trigger={null}
          collapsedWidth={76}
        >
          <Sidebar authUser={user} path={path} logout={logoutUser} />
        </Layout.Sider>
        <Layout>
          <Layout.Sider id='container-nav' width={240}>
            <Switch>
              {resourceRoutes.map((route, index) => (
                <Route key={index} path={route.path} render={route.sider} />
              ))}
              {adminRoutes.map((route, index) => (
                <Route key={index} path={route.path} render={route.sider} />
              ))}
            </Switch>
          </Layout.Sider>
          <Layout style={{ backgroundColor: '#FFFFFF' }}>
            <Layout.Content style={{ padding: '16px' }}>
              <Switch>
                <Redirect from='/' exact to='/tasks' />
                {resourceRoutes.map((route, index) => (
                  <Route
                    key={index}
                    path={route.path}
                    component={route.content}
                  />
                ))}
                {adminRoutes.map((route, index) => (
                  <Route
                    key={index}
                    path={route.path}
                    component={route.content}
                  />
                ))}
              </Switch>
            </Layout.Content>
          </Layout>
          <ReduxToastr
            timeOut={3000}
            newestOnTop={false}
            preventDuplicates
            position='top-right'
            transitionIn='fadeIn'
            transitionOut='fadeOut'
            progressBar
            closeOnToastrClick
          />
        </Layout>
      </Layout>
    )
  }
}

App.propTypes = {
  user: PropTypes.object.isRequired,
  children: PropTypes.node,
  logoutUser: PropTypes.func.isRequired
}

const wrapped = withRouter(App)

const viewsAllowed = ['/documents', '/collections', '/tasks', '/records']

const mapStateToProps = (state, { location }) => {
  const matchPath = viewsAllowed.includes(location.pathname)
    ? location.pathname
    : false
  return {
    path: location.pathname,
    user: state.auth.user,
    views: matchPath
      ? store.select.views.getViewsByType(state, matchPath)
      : null,
    isLoading: state.loading.effects.collections.create
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch.auth.logout(),
    createCollection: () => dispatch.collections.create({ name: null }),
    saveView: view => dispatch.views.update(view),
    duplicateView: id => dispatch.views.duplicate(id),
    removeView: id => dispatch.views.remove(id)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrapped)
