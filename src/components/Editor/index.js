import React, { Component } from 'react'
import propTypes from 'prop-types'
import { shouldRender, fromJson } from 'utils/render'
import { UnControlled as CodeMirror } from 'react-codemirror2'
import { Segment, Label } from 'semantic-ui-react'

import 'codemirror/mode/javascript/javascript'

class Editor extends Component {
  constructor (props) {
    super(props)
    this.state = { valid: true, code: props.code }
  }

  static propTypes = {
    code: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired,
    label: propTypes.string.isRequired
  }

  componentWillReceiveProps (props) {
    this.setState({ valid: true, code: props.code })
  }

  shouldComponentUpdate (nextProps, nextState) {
    return shouldRender(this, nextProps, nextState)
  }

  onCodeChange = (editor, metadata, code) => {
    this.setState({ valid: true, code })
    setImmediate(() => {
      try {
        this.props.onChange(fromJson(this.state.code))
      } catch (err) {
        this.setState({ valid: false, code })
      }
    })
  }

  render () {
    const { valid, code } = this.state
    const editorProps = {
      value: code,
      options: {
        theme: 'mdn-like',
        height: 'auto',
        lineNumbers: true,
        lineWrapping: true,
        indentWithTabs: false,
        tabSize: 2,
        mode: {
          name: 'javascript',
          json: true,
          statementIndent: 2
        }
      },
      autoCursor: false,
      onChange: this.onCodeChange
    }
    const labelColor = valid ? 'green' : 'red'
    const labelIcon = valid ? 'checkmark' : 'x'
    return (
      <Segment>
        <Label
          icon={labelIcon}
          content={
            this.props.label === 'ui' ? 'UI Schema View' : 'JSON Schema View'
          }
          attached='top left'
          color={labelColor}
        />
        <CodeMirror {...editorProps} />
      </Segment>
    )
  }
}

export default Editor
