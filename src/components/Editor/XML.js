import React, { Component } from 'react'
import propTypes from 'prop-types'
import { shouldRender, fromJson } from 'utils/render'
import { UnControlled as CodeMirror } from 'react-codemirror2'
import { Segment, Label } from 'semantic-ui-react'

// xml syntax highlighting
import 'codemirror/mode/xml/xml'

// auto close tags
import 'codemirror/addon/fold/xml-fold'
import 'codemirror/addon/edit/closetag'

// search addons
import 'codemirror/addon/search/search'
import 'codemirror/addon/search/searchcursor'

import 'codemirror/addon/dialog/dialog'
import 'codemirror/addon/dialog/dialog.css'

class XMLEditor extends Component {
  constructor (props) {
    super(props)
    this.state = { valid: true, code: props.code }
  }

  static propTypes = {
    code: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
  }

  componentWillReceiveProps (props) {
    this.setState({ valid: true, code: props.code })
  }

  shouldComponentUpdate (nextProps, nextState) {
    return shouldRender(this, nextProps, nextState)
  }

  onCodeChange = (editor, metadata, code) => {
    this.setState({ valid: true, code })
    setImmediate(() => {
      try {
        this.props.onChange(fromJson(this.state.code))
      } catch (err) {
        this.setState({ valid: false, code })
      }
    })
  }

  render () {
    const { valid, code } = this.state
    const editorProps = {
      value: code,
      options: {
        height: 'auto',
        lineNumbers: true,
        lineWrapping: true,
        tabSize: 2,
        autoCloseTags: true,
        mode: {
          name: 'application/xml',
          htmlMode: false
        }
      },
      onChange: this.onCodeChange
    }
    return <CodeMirror {...editorProps} />
  }
}

export default XMLEditor
