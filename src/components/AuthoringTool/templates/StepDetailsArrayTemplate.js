import React from 'react'
import { Button, Icon, Card } from 'antd'

function ActionsArrayItemTemplate (props) {
  const {
    index,
    onDropIndexClick,
    children,
    hasMoveUp,
    hasMoveDown,
    onReorderClick
  } = props

  return (
    <Card
      type='inner'
      size='small'
      title={`Action #${index}`}
      key={index}
      more={
        <Icon
          onClick={onDropIndexClick(index)}
          type='delete'
          twoToneColor='#f5222d'
          theme='twoTone'
        />
      }
    >
      {children}
    </Card>
  )
}

function ActionsArrayFieldTemplate (props) {
  const { idSchema, formContext, items } = props

  const onChange = ({ value }) => {
    if (value !== null) {
      props.onAddClick(null, value)
    }
  }

  return (
    <div className={props.className}>
      <h4>When someone completes this step...</h4>
      <Button type='primary' onClick={props.onAddClick}>
        <Icon type='plus-circle' /> New Action
      </Button>
      <div style={{ marginTop: '1em' }}>
        {items &&
          items.map((el, key) => (
            <ActionsArrayItemTemplate key={key} {...el} />
          ))}
      </div>
    </div>
  )
}

function ArrayFieldTemplate (props) {
  const { idSchema, formContext, items } = props
  return (
    <div className={props.className}>
      {props.canAdd && (
        <div className='columns'>
          <div className='column'>
            <label
              className='form-label
              '
            >
              {props.title}:
            </label>
          </div>
          <div className='column col-auto mr-2'>
            <Button onClick={props.onAddClick}>
              <Icon type='add-circle' />
            </Button>
          </div>
        </div>
      )}
      {items &&
        items.map(el => (
          <div key={el.index}>
            <div>{el.children}</div>
            <Button type='danger' block onClick={el.onDropIndexClick(el.index)}>
              <Icon type='delete' />
            </Button>
          </div>
        ))}
    </div>
  )
}

function StepDetailsArrayTemplate (props) {
  const { idSchema, formContext, items } = props
  const {
    builder: { selectedStep }
  } = formContext

  if (idSchema.$id === 'root_steps') {
    return (
      <div style={{ width: '100%' }} key={props.index}>
        {items[selectedStep].children}
      </div>
    )
  }

  if ((idSchema.$id.match(/^root_steps_\d+_actions$/) || {}).input) {
    return <ActionsArrayFieldTemplate {...props} />
  }

  return <ArrayFieldTemplate {...props} />
}

export default StepDetailsArrayTemplate
