import React from 'react'
import StepTabs from './StepTabs'

function StepDetailsTemplate ({
  TitleField,
  properties,
  description,
  DescriptionField,
  idSchema,
  addField,
  formContext,
  uiSchema,
  title,
  schema,
  formData
}) {
  const { $id } = idSchema
  const steps = properties.find(p => p.name === 'steps')

  const {
    builder: { selectedStep }
  } = formContext

  switch ($id) {
    case 'root':
      return steps.content
    case ($id.match(/^root_steps_\d+$/) || {}).input:
      return (
        <StepTabs {...{ properties, selectedStep, formContext, formData }} />
      )
    default:
      return properties.map(({ content }) => content)
  }
}

export default StepDetailsTemplate
