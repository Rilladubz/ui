import React from 'react'
import PropTypes from 'prop-types'
import { Card, Modal, Button } from 'antd'
import styles from './StepTabs.module.scss'
import { mergeObjects } from 'react-jsonschema-form/lib/utils'

class StepTabs extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    activeTab: 'general'
  }

  onTabChange = (key, type) => {
    this.setState({ activeTab: key })
  }

  componentWillUpdate = nextProps => {
    if (this.props.formData.id !== nextProps.formData.id) {
      this.setState({
        activeTab: 'general'
      })
    }
  }

  render () {
    const { activeTab } = this.state
    const { properties, selectedStep, formData } = this.props
    const tabList = {
      StartEvent: [
        { tab: 'General', key: 'general' },
        { tab: 'Conditions', key: 'conditions' }
      ],
      EndEvent: [{ tab: 'General', key: 'general' }],
      UserTask: [
        { tab: 'General', key: 'general' },
        { tab: 'Form', key: 'form' },
        { tab: 'Actions', key: 'actions' }
      ],
      ServiceTask: [
        { tab: 'General', key: 'general' },
        { tab: 'Actions', key: 'actions' }
      ]
    }

    const tabs = {
      general: properties.filter(p =>
        ['name', 'type', 'documentation', 'users', 'groups'].includes(p.name)
      ),
      conditions: properties.filter(p => ['condition'].includes(p.name)),
      form: properties.filter(p => ['form'].includes(p.name)),
      actions: properties.filter(p => ['actions', 'action'].includes(p.name))
    }

    return (
      <Card
        className='step-details'
        size='small'
        title={`Step Details - ID: ${formData.id}`}
        tabList={tabList[formData.type]}
        activeTabKey={activeTab}
        onTabChange={key => this.onTabChange(key)}
      >
        {activeTab !== 'form' ? (
          <div style={{ padding: '1em 1.5em' }}>
            {tabs[activeTab].map(({ content }) => content)}
          </div>
        ) : (
          tabs[activeTab].map(({ content }) => content)
        )}
      </Card>
    )
  }
}

export default StepTabs
