import React from 'react'
import classnames from 'classnames'
import ScrollIntoViewIfNeeded from 'react-scroll-into-view-if-needed'

import { Card, Tile } from 'components/Atoms'

import styles from './StepNav.module.scss'
import NodeView from './NodeView'
import StepMarker from './StepMarker'

const StepNav = ({ checklist, selected, onStepClick }) => {
  return (
    <Card className={styles.default}>
      <Card.Header>
        <div id='step-tools-portal' />
      </Card.Header>
      <Card.Body>
        <div className='columns'>
          <NodeView
            className='node-view'
            steps={checklist.steps}
            selected={selected}
            onStepClick={onStepClick}
          />
          <div className='step-nav-menu'>
            <StepMarker
              onClick={() => onStepClick(null)}
              key='root'
              type='root'
              title={checklist.title || '*Untitled Checklist'}
              selected={selected === null}
            />
            {checklist.steps.map((v, k) => {
              return (
                <ScrollIntoViewIfNeeded active={k === selected} key={k}>
                  <StepMarker
                    onClick={() => onStepClick(k)}
                    centered
                    selected={selected === k}
                    index={k + 1}
                    type={v.type}
                    title={v.title}
                  />
                </ScrollIntoViewIfNeeded>
              )
            })}
          </div>
        </div>
      </Card.Body>
    </Card>
  )
}

export default StepNav
