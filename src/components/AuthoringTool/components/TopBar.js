import React from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'

import { Navbar, Button, Modal } from 'components/Atoms'
import { Preview_icon, ArrowLeft_icon } from 'modules/icons/miraIcons'
import { styleVars } from 'modules/styleVars'

import styles from './TopBar.module.scss'

class TopBar extends React.Component {
  state = {
    modalOpen: false
  }

  setModal = (modalOpen = false, cb) => {
    this.setState(
      {
        modalOpen
      },
      cb
    )
  }

  renderModal = () => {
    const { publish } = this.props
    const footer = (
      <div className='form-actions'>
        <Button onClick={() => this.setModal(false)}>Cancel</Button>
        <Button primary onClick={() => this.setModal(false, publish)}>
          Confirm
        </Button>
      </div>
    )

    return (
      <Modal overlay active title='Publish Checklist' footer={footer}>
        <p>Are you sure you want to publish this checklist?</p>
      </Modal>
    )
  }

  render () {
    const { title, publish, onSave } = this.props
    const { modalOpen } = this.state
    return (
      <Navbar className={styles.default}>
        <Navbar.Section>
          <Link to='/compliance/checklists'>
            <ArrowLeft_icon size={32} fill={styleVars.miraMatteBlack} />
          </Link>
          <span className='title'>{title || '*Untitled Checklist'}</span>
        </Navbar.Section>
        <Navbar.Section>
          <Button secondary className='px-2'>
            <Preview_icon fill={styleVars.miraMatteBlack} size={28} />
          </Button>
          <Button onClick={onSave} primary>
            Save
          </Button>
          <Button
            disabled={!publish}
            onClick={publish ? () => this.setModal(true) : undefined}
            secondary
          >
            Publish
          </Button>
        </Navbar.Section>
        {modalOpen ? this.renderModal() : null}
      </Navbar>
    )
  }
}

export default TopBar
