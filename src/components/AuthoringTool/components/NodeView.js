import React from 'react'
import ReactDOM from 'react-dom'

import DagreD3, { d3 } from './DagreD3'
import styles from './NodeView.module.scss'
import cx from 'classnames'

class NodeView extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      resize: true,
      height: 0,
      width: 0
    }
  }

  componentDidMount () {
    if (this.sizer) {
      const size = this.sizer.getBoundingClientRect()
      this.setState({
        resize: false,
        height: size.height,
        width: size.width
      })
    }
  }

  componentWillUpdate (next) {
    const { selected, steps } = this.props
    if (selected !== next.selected || steps !== next.steps) {
      this.setState({
        resize: true
      })
    }
  }

  componentDidUpdate () {
    const size = this.sizer.getBoundingClientRect()
    if (this.state.resize) {
      this.setState({
        resize: false,
        height: size.height,
        width: size.width
      })
    }
  }

  render () {
    const { steps, className, selected, onStepClick } = this.props
    const { height, width } = this.state
    const nodes = {
      root: {
        id: 'root',
        type: 'root',
        width: 2,
        height: 2,
        label: '',
        class: cx(styles.root, { active: selected === null }),
        shape: 'circle'
      }
    }
    const edges = []

    steps.forEach((v, k) => {
      nodes[`step_${k}`] = {
        id: `step_${k}`,
        width: 2,
        height: 2,
        label: '',
        type: v.type,
        class: cx(styles[v.type], { active: k === selected }),
        shape: 'circle'
      }
    })

    steps.forEach((v, k) => {
      const firstStep = k === 0
      const lastStep = k === steps.length - 1

      if (firstStep) {
        edges.push([
          'root',
          'step_0',
          {
            id: `rootstep_0`,
            class: styles.edge,
            weight: 100,
            minlen: 1,
            arrowhead: 'undirected'
          }
        ])
      }

      if (!lastStep) {
        const hasActions = v.actions.length
        const lastPath = firstStep
          ? edges.find(n => n[2].id === 'rootstep_0')
          : edges.find(n => n[1] === `step_${k}`)

        let nextPath = lastPath
          ? [
            [
              `step_${k}`,
              `step_${k + 1}`,
              {
                id: `step_${k}step_${k + 1}`,
                class: styles.edge,
                arrowhead: 'undirected'
              }
            ]
          ]
          : null

        if (hasActions) {
          v.actions.forEach(a => {
            if (a.type === 'jump') {
              const targetIdx = steps.findIndex(s => s.id === a.to)
              if (targetIdx > -1) {
                nextPath = [
                  [
                    `step_${k}`,
                    `step_${targetIdx}`,
                    {
                      id: `step_${k}step_${targetIdx}`,
                      class: styles.jump,
                      arrowhead: 'undirected'
                    }
                  ]
                ]
              }
            }

            if (a.type === 'comparison') {
              if (a.action && a.action.type === 'jump') {
                const targetIdx = steps.findIndex(s => s.id === a.action.to)
                if (targetIdx > -1) {
                  nextPath.push([
                    `step_${k}`,
                    `step_${targetIdx}`,
                    {
                      id: `step_${k}step_${targetIdx}`,
                      class: styles.jump,
                      marker: {
                        markerUnits: '1px'
                      }
                    }
                  ])
                }
              }
            }
          })
        }
        nextPath && Array.prototype.push.apply(edges, nextPath)
      }
    })

    return (
      <div className={className} ref={c => (this.sizer = c)}>
        <DagreD3
          graph={{
            rankdir: 'TB',
            align: 'UL',
            nodeSep: 16,
            rankSep: 28,
            edgeSep: 11
          }}
          height={height}
          width={width}
          nodes={nodes}
          edges={edges}
          onNodeClick={(d, i, g) => {
            d === 'root' ? onStepClick(null) : onStepClick(i - 1)
          }}
        />
      </div>
    )
  }
}

export default NodeView
