import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Button } from 'antd'
import { Record, fromJS } from 'immutable'
import shortid from 'shortid'

import { Form } from 'components/JSONschema'
import {
  WorkflowTemplate,
  StepsArrayTemplate,
  StepDetailsTemplate,
  StepDetailsArrayTemplate,
  FieldTemplate
} from './templates'
import { StepsArrayField, StepDetailsArrayField, JsonFormField } from './fields'
import * as widgets from './widgets'
import { WorkflowMeta } from './components'
import styles from './WorkflowBuilder.module.scss'
import { WorkflowSchema, WorkflowUiSchema } from './'

class WorkflowBuilder extends PureComponent {
  constructor (props) {
    super(props)
    const schema = props.item ? JSON.parse(props.item.schema) : WorkflowSchema
    const uiSchema = props.item
      ? JSON.parse(props.item.uiSchema)
      : WorkflowUiSchema
    const InitData = Record({
      schema: fromJS(schema),
      uiSchema: fromJS(uiSchema),
      workflow: fromJS(
        props.item
          ? JSON.parse(props.item.workflow)
          : {
            title: '',
            steps: [
              {
                id: `StartEvent_${shortid.generate()}`,
                name: '',
                type: 'StartEvent',
                documentation: '',
                condition: {
                  allowManual: false
                }
              },
              {
                id: `EndEvent_${shortid.generate()}`,
                name: '',
                type: 'EndEvent',
                documentation: ''
              }
            ]
          }
      ),
      selectedStep: 0
    })
    this.state = {
      data: new InitData()
    }
  }

  static contextTypes = {
    router: PropTypes.object
  }

  setImmState = fn => {
    this.setState(({ data }) => ({
      data: fn(data)
    }))
  }

  selectStep = id => {
    this.setImmState(s => s.set('selectedStep', id))
  }

  handleSave = () => {
    const { data } = this.state
    const { workflow } = data.toJS()
    this.props.onSave(workflow)
  }

  componentDidMount () {
    const { schema } = this.state.data
    const { users, roles, documentTypes, recordTypes } = this.props
    this.setImmState(s =>
      s.set(
        'schema',
        schema.withMutations(_schema => {
          _schema
            .setIn(
              ['definitions', 'UsersList', 'enum'],
              users.map(v => v.get('id'))
            )
            .setIn(
              ['definitions', 'UsersList', 'enumNames'],
              users.map(v => `${v.get('firstName')} ${v.get('lastName')}`)
            )
            .setIn(
              ['definitions', 'GroupsList', 'enum'],
              roles.map(v => v.get('id'))
            )
            .setIn(
              ['definitions', 'GroupsList', 'enumNames'],
              roles.map(v => v.get('name'))
            )
            .setIn(
              ['definitions', 'DocumentTypesList', 'enum'],
              documentTypes.map(v => v.get('id'))
            )
            .setIn(
              ['definitions', 'DocumentTypesList', 'enumNames'],
              documentTypes.map(v => v.get('name'))
            )
            .setIn(
              ['definitions', 'RecordTypesList', 'enum'],
              recordTypes.map(v => v.get('id'))
            )
            .setIn(
              ['definitions', 'RecordTypesList', 'enumNames'],
              recordTypes.map(v => v.get('name'))
            )
        })
      )
    )
  }

  updateChecklist = props => {
    const { formData } = props

    this.setImmState(s => {
      let _s = s
      if (formData.steps.length !== _s.getIn(['workflow', 'steps']).count()) {
        if (formData.steps.length) {
          const focusIdx = formData.steps.findIndex(f => f.focused)
          if (focusIdx > -1) {
            _s = _s.set('selectedStep', focusIdx)
            delete formData.steps[focusIdx].focused
          } else {
            _s = _s.set('selectedStep', _s.selectedStep - 1)
          }
        } else {
          _s = _s.set('selectedStep', 0)
        }
      }

      _s = _s.set('workflow', fromJS(formData))
      return _s
    })
  }

  renderStepDetails = () => {
    const { recordTypes } = this.props
    const { data } = this.state
    const dataJS = data.toJS()
    const stepProps = {
      schema: dataJS.schema,
      uiSchema: dataJS.uiSchema,
      showErrorList: false,
      noHtml5Validate: true,
      noValidate: true,
      fields: {
        StepsArrayField: StepDetailsArrayField,
        JsonFormField: JsonFormField
      },
      formContext: {
        builder: {
          selectStep: this.selectStep,
          selectedStep: dataJS.selectedStep,
          recordTypes
        },
        recordTypes
      },
      widgets,
      formData: dataJS.workflow,
      ObjectFieldTemplate: StepDetailsTemplate,
      ArrayFieldTemplate: StepDetailsArrayTemplate,
      FieldTemplate,
      onChange: this.updateChecklist
    }
    return <Form {...stepProps} />
  }

  render () {
    const { data } = this.state
    const dataJS = data.toJS()
    const checklistProps = {
      schema: dataJS.schema,
      uiSchema: dataJS.uiSchema,
      showErrorList: false,
      noHtml5Validate: true,
      formContext: {
        builder: {
          selectStep: this.selectStep,
          selectedStep: dataJS.selectedStep
        }
      },
      noValidate: true,
      formData: dataJS.workflow,
      fields: {
        StepsArrayField: StepsArrayField
      },
      ArrayFieldTemplate: StepsArrayTemplate,
      ObjectFieldTemplate: WorkflowTemplate,
      FieldTemplate,
      onChange: this.updateChecklist
    }

    return (
      <div className={styles.default}>
        <div className='right-toolbar'>
          <Button onClick={this.handleSave}>Generate Workflow</Button>
        </div>
        <Row gutter={16}>
          <Col span={6}>
            <Form {...checklistProps} />
          </Col>
          <Col span={18} className={styles.meta}>
            {dataJS.selectedStep !== null ? (
              this.renderStepDetails()
            ) : (
              <WorkflowMeta />
            )}
          </Col>
        </Row>
      </div>
    )
  }
}

export default WorkflowBuilder
