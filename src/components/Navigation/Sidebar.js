import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Menu, Avatar } from 'antd'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import HelpDrawerContent from '../../containers/Help'

import {
  faCog,
  faUsersCog,
  faUserCheck,
  faKey,
  faFileInvoice,
  faFile,
  faTasks,
  faDatabase,
  faLayerGroup,
  faSignOutAlt,
  faQuestion
} from '@fortawesome/free-solid-svg-icons'
library.add(
  faCog,
  faUsersCog,
  faUserCheck,
  faKey,
  faFileInvoice,
  faFile,
  faTasks,
  faDatabase,
  faLayerGroup,
  faSignOutAlt,
  faQuestion
)

const routeKeys = {
  documents: 'documents',
  records: 'records',
  tasks: 'tasks',
  collections: 'collections',
  account: 'account',
  workflows: 'settings',
  'document-types': 'settings',
  users: 'settings',
  roles: 'settings',
  permissions: 'settings',
  'record-types': 'settings'
}

class Sider extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }

  state = { visible: false }

  showDrawer = () => {
    this.setState({
      visible: true
    })
  }

  onClose = cb => {
    this.setState(
      {
        visible: false
      },
      () => {
        setTimeout(() => {
          cb()
        }, 1000)
      }
    )
  }

  goTo = location => {
    this.context.router.history.push(location)
  }

  render () {
    const { path, authUser, logout } = this.props
    const openMenu = path ? [routeKeys[path.split('/')[1]]] : []
    const initials = authUser.lastName
      ? `${authUser.firstName[0]}${authUser.lastName[0]}`
      : authUser.firstName[0]
    return (
      <Fragment>
        <Menu
          mode='inline'
          inlineCollapsed
          theme='dark'
          selectedKeys={openMenu}
        >
          <Menu.Item onClick={() => this.goTo('/tasks')} key='tasks'>
            <FontAwesomeIcon
              style={{ fontSize: '1.4em' }}
              className='anticon'
              icon='tasks'
            />
            <span>Tasks</span>
          </Menu.Item>
          <Menu.Item onClick={() => this.goTo('/documents')} key='documents'>
            <FontAwesomeIcon
              style={{ fontSize: '1.4em' }}
              className='anticon'
              icon='file'
            />
            <span>Documents</span>
          </Menu.Item>
          <Menu.Item onClick={() => this.goTo('/records')} key='records'>
            <FontAwesomeIcon
              style={{ fontSize: '1.4em' }}
              className='anticon'
              icon='database'
            />
            <span>Records</span>
          </Menu.Item>
          <Menu.Item
            onClick={() => this.goTo('/collections')}
            key='collections'
          >
            <FontAwesomeIcon
              style={{ fontSize: '1.4em' }}
              className='anticon'
              icon='layer-group'
            />
            <span>Collections</span>
          </Menu.Item>
        </Menu>
        <Menu
          inlineCollapsed
          mode='inline'
          theme='dark'
          style={{ position: 'fixed', bottom: 0 }}
          selectedKeys={openMenu}
        >
          <Menu.Item onClick={this.showDrawer}>
            <FontAwesomeIcon
              style={{ fontSize: '1.4em' }}
              className='anticon'
              icon='question'
            />
            <span>Help</span>
            <HelpDrawerContent
              handleOnClose={this.onClose}
              isVisible={this.state.visible}
            />
          </Menu.Item>
          <Menu.Item key='settings' onClick={() => this.goTo('/users')}>
            <FontAwesomeIcon
              style={{ fontSize: '1.4em' }}
              className='anticon'
              icon='cog'
            />
            <span>Settings</span>
          </Menu.Item>
          <Menu.Item key='account'>
            <Avatar
              id='account-avatar'
              className='anticon'
              shape='square'
              style={{ backgroundColor: '#89D578' }}
            >
              {initials}
            </Avatar>
            <span>My Account</span>
          </Menu.Item>
          <Menu.Item key='logout' onClick={() => logout()}>
            <FontAwesomeIcon
              style={{ fontSize: '1.4em' }}
              className='anticon'
              icon='sign-out-alt'
            />
            <span>Sign Out</span>
          </Menu.Item>
        </Menu>
      </Fragment>
    )
  }
}

export default Sider
