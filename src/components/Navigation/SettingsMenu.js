import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Menu, Layout } from 'antd'

class SettingsMenu extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }

  goTo = location => {
    this.context.router.history.push(location)
  }

  render () {
    const { location } = this.props
    const matchPath = location.pathname
    return (
      <Fragment>
        <Layout.Header>Settings</Layout.Header>
        <Layout.Content>
          <div
            className='menu-wrapper'
            style={{ height: 'calc(100vh - 64px)' }}
          >
            <Menu mode='inline' theme='dark' selectedKeys={[matchPath]}>
              <Menu.ItemGroup
                key='access-authorization'
                title='Access & Authorization'
              >
                <Menu.Item key='/users' onClick={() => this.goTo('/users')}>
                  Users
                </Menu.Item>
                <Menu.Item key='/roles' onClick={() => this.goTo('/roles')}>
                  Roles
                </Menu.Item>
                <Menu.Item
                  key='/permissions'
                  onClick={() => this.goTo('/permissions')}
                >
                  Permissions
                </Menu.Item>
              </Menu.ItemGroup>
              <Menu.ItemGroup key='documents-data' title='Documents & Data'>
                <Menu.Item
                  key='/pending-files'
                  onClick={() => this.goTo('/pending-files')}
                >
                  Pending Files
                </Menu.Item>
                <Menu.Item
                  key='/document-types'
                  onClick={() => this.goTo('/document-types')}
                >
                  Document Types
                </Menu.Item>
                <Menu.Item
                  key='/record-types'
                  onClick={() => this.goTo('/record-types')}
                >
                  Record Types
                </Menu.Item>
              </Menu.ItemGroup>
              <Menu.ItemGroup key='automation' title='Automation'>
                <Menu.Item
                  key='/workflows'
                  onClick={() => this.goTo('/workflows')}
                >
                  Workflows
                </Menu.Item>
              </Menu.ItemGroup>
            </Menu>
          </div>
        </Layout.Content>
      </Fragment>
    )
  }
}

export default SettingsMenu
