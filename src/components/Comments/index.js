import React, { Component } from 'react'
import propTypes from 'prop-types'
import { Comment, Form, Segment } from 'semantic-ui-react'

export default class Comments extends Component {
  static propTypes = {
    collectionId: propTypes.number.isRequired,
    comments: propTypes.object.isRequired,
    createComment: propTypes.func.isRequired
    // deleteComment: propTypes.func.isRequired,
    // updateComment: propTypes.func.isRequired,
  }

  state = {
    target: 'collection',
    body: ''
  }

  replyTo = target => {
    this.setState({
      target
    })
  }

  submitComment = () => {
    const { body, target } = this.state
    const { createComment, collectionId } = this.props
    const newComment = {
      body
    }
    if (target !== 'collection') {
      newComment.parent = parseInt(target)
    }
    createComment(collectionId, newComment).then(() => {
      this.setState({
        body: '',
        target: 'collection'
      })
    })
  }

  handleChange = (e, { value }) => {
    this.setState({
      body: value
    })
  }

  renderForm = () => {
    const { body, target } = this.state
    const btnProps = {
      content: target === 'collection' ? 'Add Comment' : 'Add Reply',
      type: 'submit',
      labelPosition: 'left',
      icon: 'edit',
      size: 'small'
    }
    const cancelBtn = (
      <Form.Button
        size='small'
        onClick={() => this.replyTo('collection')}
        content='Cancel'
        negative
      />
    )
    return (
      <Form style={{ marginTop: '1em' }} onSubmit={() => this.submitComment()}>
        <Form.TextArea rows={4} value={body} onChange={this.handleChange} />
        <Form.Group>
          <Form.Button {...btnProps} primary />
          {target !== 'collection' ? cancelBtn : null}
        </Form.Group>
      </Form>
    )
  }

  render () {
    const { comments } = this.props
    const { target } = this.state
    const items = comments.length
      ? comments
        .sortBy(c => c.id)
        .toJS()
        .map((c, k) => (
          <Comment key={k}>
            <Segment>
              <Comment.Content>
                <Comment.Author as='a'>{c.creator.name}</Comment.Author>
                <Comment.Metadata>
                  <div>{c.updatedAt}</div>
                </Comment.Metadata>
                <Comment.Text>{c.body}</Comment.Text>
                <Comment.Actions>
                  <Comment.Action as='a' onClick={() => this.replyTo(c.id)}>
                      Reply
                  </Comment.Action>
                </Comment.Actions>
                {target === c.id ? this.renderForm() : null}
              </Comment.Content>
            </Segment>
          </Comment>
        ))
      : null

    return (
      <Comment.Group size='small'>
        {items}
        {target === 'collection' ? this.renderForm() : null}
      </Comment.Group>
    )
  }
}
