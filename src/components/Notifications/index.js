import React, { Component } from 'react'
import propTypes from 'prop-types'
import Pusher from 'react-pusher'
import { Menu, Label, Icon, Popup, Message } from 'semantic-ui-react'

export default class Notifications extends Component {
  static propTypes = {
    channel: propTypes.string.isRequired,
    onUpdate: propTypes.func.isRequired,
    notifs: propTypes.object
  }

  render () {
    const { channel, onUpdate, notifs } = this.props
    const count = notifs ? notifs.length : 0
    const trigger = (
      <Menu.Item as='a'>
        <Icon name='bell' /> Notifications
        <Label color='teal' content={count} />
      </Menu.Item>
    )
    const items = notifs.map((n, k) => (
      <Message compact key={k} size='tiny'>
        {n.data.message}
      </Message>
    ))
    return (
      <div>
        <Popup trigger={trigger} on='click' position='bottom center'>
          {items || 'No Notifications'}
        </Popup>
        <Pusher channel={channel} onUpdate={onUpdate} event='notification' />
      </div>
    )
  }
}
