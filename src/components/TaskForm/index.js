import React from 'react'
import { Form } from 'semantic-ui-react'
import { DateTimeInput } from 'semantic-ui-calendar-react'

const fieldTypes = {
  string: Form.Input,
  enum: Form.Dropdown,
  boolean: Form.Radio,
  long: Form.Input,
  date: DateTimeInput
}
