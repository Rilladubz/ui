import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ContainerLoader } from 'components/Loader'
import { Layout, Divider } from 'antd'
import ResourceForm from './ResourceForm'
import ResourceNotFound from './ResourceNotFound'

class ResourceEditView extends Component {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.func,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  goTo (location) {
    this.context.router.history.push(location)
  }

  componentDidMount () {
    const { prefetch, resourceName, match } = this.props
    if (prefetch.length && match.path === `${resourceName.route}/:id`) {
      prefetch.forEach(p => p())
    }
  }

  componentDidUpdate (prevProps) {
    const { prefetch, match } = this.props
    if (prefetch.length && prevProps.match.params.id !== match.params.id) {
      prefetch.forEach(p => p())
    }
  }

  renderContent = () => {
    const {
      resourceName,
      resourceForm,
      item,
      dependencies,
      ...rest
    } = this.props
    const formProps = {
      resourceName,
      resourceForm,
      init: resourceForm.beforeEdit
        ? resourceForm.beforeEdit(dependencies, item.toJS())
        : item.toJS(),
      ...rest
    }
    formProps.resourceName.singular === 'Role'
      ? (formProps.dependencies = {
        ...dependencies,
        permissions: dependencies.permissions.toJS()
      })
      : (formProps.dependencies = dependencies)
    return (
      <Layout style={{ backgroundColor: 'transparent', height: '100%' }}>
        <Layout.Header style={{ backgroundColor: 'transparent', height: 100 }}>
          <h1>
            Edit {resourceName.singular} - {item.id}
          </h1>
          <Divider />
        </Layout.Header>
        <Layout.Content>
          <ResourceForm {...formProps} />
        </Layout.Content>
      </Layout>
    )
  }

  render () {
    const { loading, saving, resourceName, item } = this.props
    if (loading && !saving) {
      return <ContainerLoader />
    } else {
      return item ? this.renderContent() : <ResourceNotFound />
    }
  }
}

export default ResourceEditView
