import React from 'react'
import { Form, Segment, Header, Icon } from 'semantic-ui-react'

const ResourceNotFound = () => (
  <Segment placeholder>
    <Header icon>
      <Icon name='warning circle' />
      The item you requested could not be found.
    </Header>
  </Segment>
)

export default ResourceNotFound
