import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form } from 'components/JSONschema'

class ResourceForm extends Component {
  static propTypes = {
    resourceName: PropTypes.object,
    resourceForm: PropTypes.func,
    init: PropTypes.object,
    dependencies: PropTypes.object,
    saving: PropTypes.bool,
    saveAction: PropTypes.func
  }

  static contextTypes = {
    router: PropTypes.object
  }

  handleSave = ({ formData }) => {
    const { saveAction, resourceName } = this.props
    saveAction(formData).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  checkRule (rule, formData) {
    switch (rule.type) {
      case 'equalsProp':
        return formData[rule.key] === formData[rule.matchProp]
      default:
        return true
    }
  }

  validate = (formData, errors) => {
    const { resourceForm, dependencies } = this.props
    const { schema, uiSchema } = resourceForm(dependencies)
    if (schema.validations) {
      schema.validations.forEach(v => {
        let rule = this.checkRule(v, formData)
        if (!rule) {
          errors[v.key].addError(v.message)
        }
      })
    }
    return errors
  }

  render () {
    const {
      resourceForm,
      resourceName,
      dependencies,
      saving,
      init
    } = this.props
    const { schema, uiSchema } = resourceForm(dependencies)
    const formProps = {
      validate: this.validate,
      onSubmit: this.handleSave,
      schema,
      uiSchema,
      noHtml5Validate: true,
      showErrorList: false,
      formData: init
    }
    return <Form {...formProps} />
  }
}

export default ResourceForm
