import React from 'react'
import { Form, Segment, Header, Icon } from 'semantic-ui-react'

const ResourceListEmpty = ({ name }) => (
  <Segment placeholder>
    <Header size='large' icon>
      <Icon color='blue' name='info circle' />
      No {name.plural} found. Try creating a new {name.singular}.
    </Header>
  </Segment>
)

export default ResourceListEmpty
