import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Divider, Segment, Grid } from 'semantic-ui-react'
import { Form, Input, Select, Radio, Row, Col, Button } from 'antd'
import { DateTimeInput } from 'semantic-ui-calendar-react'

const inputMap = {
  Input: Input,
  Select: Select,
  Radio: Radio,
  DateTime: DateTimeInput
}

class ResourceForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      form: props.init
    }
  }

  static propTypes = {
    resourceName: PropTypes.object,
    resourceForm: PropTypes.object,
    init: PropTypes.object,
    dependencies: PropTypes.object,
    saving: PropTypes.bool,
    saveAction: PropTypes.func
  }

  static contextTypes = {
    router: PropTypes.object
  }

  handleSave = () => {
    const { form } = this.state
    const { saveAction, resourceName } = this.props
    saveAction(form).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  updateForm = (e, { name, value }) => {
    const { onSelect } = this.props
    const data = {}
    data[name] = value
    if (onSelect && onSelect.hasOwnProperty(name)) {
      this.setState(
        {
          form: {
            ...this.state.form,
            ...data
          }
        },
        () => onSelect[name](value)
      )
    } else {
      this.setState({
        form: {
          ...this.state.form,
          ...data
        }
      })
    }
  }

  render () {
    const { form } = this.props
    const { getFieldDecorator } = form
    const { resourceForm, resourceName, dependencies, saving } = this.props
    const { fields, validations } = resourceForm
    const groups = fields.map((group, k) => {
      const inputs = group.map((v, k) => {
        let Field = inputMap[v.component]
        let input
        if (v.component === 'Dropdown') {
          const options = v.getOptions(dependencies, form)
          Field = (
            <Field>
              {options.map(o => (
                <Select.Option value={o.value}>{o.text}</Select.Option>
              ))}
            </Field>
          )
        }

        // if (v.component === 'Radio') {
        //   v.props.checked = form[v.name] === v.initialValue
        // }
        // if (v.props.depends) {
        //   v.fieldProps.disabled = !form[v.depends]
        // }
        // const value =
        //   v.getValue && !v.fieldProps.disabled
        //     ? v.getValue(dependencies, form)
        //     : form[v.name]
        return (
          <Col>
            <Form.Item {...v.itemProps} key={k}>
              {getFieldDecorator(v.name, { rules: v.rules })(
                <Field {...v.fieldProps} name={v.name} />
              )}
            </Form.Item>
          </Col>
        )
      })
      return <Row>{inputs}</Row>
    })

    return (
      <Grid.Column>
        <Segment padded loading={saving}>
          <Form onSubmit={this.handleSave}>{groups}</Form>
          <Divider clearing hidden />
        </Segment>
      </Grid.Column>
    )
  }
}

export default Form.create()(ResourceForm)
