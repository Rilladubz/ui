import React from 'react'
import PropTypes from 'prop-types'
import { asNumber, guessType } from 'react-jsonschema-form/lib/utils'
import { Select } from 'antd'

const nums = new Set(['number', 'integer'])

/**
 * This is a silly limitation in the DOM where option change event values are
 * always retrieved as strings.
 */
function processValue (schema, value) {
  // "enum" is a reserved word, so only "type" and "items" can be destructured
  const { type, items } = schema
  if (value === '') {
    return undefined
  } else if (type === 'array' && items && nums.has(items.type)) {
    return value.map(asNumber)
  } else if (type === 'boolean') {
    return value === 'true'
  } else if (type === 'number') {
    return asNumber(value)
  }

  // If type is undefined, but an enum is present, try and infer the type from
  // the enum values
  if (schema.enum) {
    if (schema.enum.every(x => guessType(x) === 'number')) {
      return asNumber(value)
    } else if (schema.enum.every(x => guessType(x) === 'boolean')) {
      return value === 'true'
    }
  }

  return value
}

function getValue (event, multiple) {
  if (multiple) {
    return [].slice
      .call(event.target.options)
      .filter(o => o.selected)
      .map(o => o.value)
  } else {
    return event.target.value
  }
}

function SelectWidget (props) {
  const {
    schema,
    id,
    options,
    value,
    required,
    disabled,
    readonly,
    multiple,
    autofocus,
    onChange,
    onBlur,
    onFocus
  } = props
  const { enumOptions, enumDisabled, mode, placeholder } = options
  const emptyValue = multiple ? [] : ''
  const selectProps = {
    id,
    placeholder,
    disabled: disabled || readonly,
    autofocus: autofocus,
    required,
    size: 'large',
    value: typeof value === 'undefined' ? emptyValue : value,
    mode: mode || 'default'
  }
  return (
    <Select
      {...selectProps}
      onChange={value => {
        onChange(processValue(schema, value))
      }}
    >
      {enumOptions.map(({ value, label }, i) => {
        const disabled = enumDisabled && enumDisabled.indexOf(value) != -1
        return (
          <Select.Option key={i} value={value} disabled={disabled}>
            {label}
          </Select.Option>
        )
      })}
    </Select>
  )
}

SelectWidget.defaultProps = {
  autofocus: false
}

SelectWidget.propTypes = {
  schema: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  options: PropTypes.shape({
    enumOptions: PropTypes.array
  }).isRequired,
  value: PropTypes.any,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  readonly: PropTypes.bool,
  multiple: PropTypes.bool,
  autofocus: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func
}

export default SelectWidget
