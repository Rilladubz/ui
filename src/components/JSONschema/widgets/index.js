import BaseInput from './BaseInput'
import SelectWidget from './SelectWidget'
import CheckboxWidget from './CheckboxWidget'
import TextareaWidget from './TextareaWidget'

export default {
  BaseInput,
  SelectWidget,
  CheckboxWidget,
  TextareaWidget
}
