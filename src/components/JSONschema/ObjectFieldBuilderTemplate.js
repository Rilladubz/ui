import React from 'react'
import { Menu, Dropdown, Button, Row, Col } from 'antd'

function ObjectFieldTemplate ({
  TitleField,
  properties,
  title,
  description,
  id,
  addField,
  formContext,
  preview
}) {
  const { builder } = formContext
  const menu = (
    <Menu onClick={builder.addField}>
      <Menu.Item key='string'>Text</Menu.Item>
      <Menu.Item key='number'>Number</Menu.Item>
      <Menu.Item key='boolean'>Boolean</Menu.Item>
    </Menu>
  )
  return (
    <div>
      {id !== 'root' ? <TitleField title={title} /> : null}
      {description}
      {properties.map(({ content }) => content)}
      <Row style={formContext.builder.preview ? { display: 'none' } : {}}>
        <Col span={10} offset={7}>
          <Dropdown overlay={menu} trigger={['click']}>
            <Button type='dashed' block icon='plus-circle'>
              Add Field
            </Button>
          </Dropdown>
        </Col>
      </Row>
    </div>
  )
}

export default ObjectFieldTemplate
