import React from 'react'
import classnames from 'classnames'
import { Form } from 'antd'
import { getUiOptions } from 'react-jsonschema-form/lib/utils'

function DefaultTemplate (props) {
  const {
    id,
    label,
    children,
    errors,
    help,
    description,
    hidden,
    required,
    displayLabel,
    formContext,
    classNames,
    rawErrors,
    rawHelp,
    uiSchema,
    schema,
    ...otherProps
  } = props
  if (hidden) {
    return children
  }
  const { labelCol, wrapperCol } = getUiOptions(uiSchema)
  let className = classNames.replace('form-group', '').trim()
  const formGroupProps = {
    id,
    className: classnames(className),
    help: rawHelp,
    label: displayLabel ? label : false,
    labelCol: { span: labelCol || 6 },
    wrapperCol: { span: wrapperCol || 14 }
  }

  if (rawErrors && rawErrors.length) {
    formGroupProps.validateStatus = 'error'
    formGroupProps.help = rawErrors[0]
    formGroupProps.hasFeedback = schema.type !== 'boolean'
  }

  if (id === 'root') {
    return (
      <div id={id} className={classnames('field', 'field-object')}>
        {children}
      </div>
    )
  }

  return <Form.Item {...formGroupProps}>{children}</Form.Item>
}

export default DefaultTemplate
