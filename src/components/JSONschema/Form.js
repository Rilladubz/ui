import React from 'react'
import JSONForm from 'react-jsonschema-form'
import { getDefaultRegistry } from 'react-jsonschema-form/lib/utils'
import classnames from 'classnames'

import { Form as AntForm, Button } from 'antd'
import ErrorList from './ErrorList'
import ObjectFieldTemplate from './ObjectFieldTemplate'
import FieldTemplate from './FieldTemplate'
import origamiWidgets from './widgets'
import * as Fields from './fields'

export default class Form extends JSONForm {
  static defaultProps = {
    builder: false,
    ErrorList: ErrorList,
    ...Form.defaultProps
  }

  getRegistry () {
    const { fields, widgets } = getDefaultRegistry()
    const arrayTemplate = this.props.ArrayFieldTemplate
      ? { ArrayFieldTemplate: this.props.ArrayFieldTemplate }
      : {}
    return {
      fields: { ...fields, ...Fields, ...this.props.fields },
      widgets: { ...widgets, ...origamiWidgets, ...this.props.widgets },
      FieldTemplate: this.props.FieldTemplate || FieldTemplate,
      ObjectFieldTemplate:
        this.props.ObjectFieldTemplate || ObjectFieldTemplate,
      ...arrayTemplate,
      definitions: this.props.schema.definitions || {},
      formContext: this.props.formContext || {}
    }
  }

  render () {
    const {
      children,
      safeRenderCompletion,
      id,
      idPrefix,
      className,
      name,
      method,
      target,
      action,
      autocomplete,
      enctype,
      acceptcharset,
      noHtml5Validate,
      disabled,
      buttonLabel,
      buttonCol,
      formContext,
      onSubmit
    } = this.props
    const { schema, uiSchema, formData, errorSchema, idSchema } = this.state
    const registry = this.getRegistry()
    const { SchemaField } = registry.fields
    const buttonNode = onSubmit ? (
      <AntForm.Item wrapperCol={{ span: 8, offset: 6 }}>
        <Button type='primary' htmlType='submit' size='large'>
          {buttonLabel || 'Submit'}
        </Button>
      </AntForm.Item>
    ) : null
    return (
      <AntForm
        className={classnames(className, 'rjsf')}
        layout='horizontal'
        id={id}
        name={name}
        method={method}
        target={target}
        action={action}
        autoComplete={autocomplete}
        encType={enctype}
        acceptCharset={acceptcharset}
        noValidate={noHtml5Validate}
        onSubmit={this.onSubmit}
        ref={form => {
          this.formElement = form
        }}
      >
        {this.renderErrors()}
        <SchemaField
          schema={schema}
          uiSchema={uiSchema}
          errorSchema={errorSchema}
          idSchema={idSchema}
          idPrefix={idPrefix}
          formData={formData}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          registry={registry}
          safeRenderCompletion={safeRenderCompletion}
          disabled={disabled}
        />
        {children || buttonNode}
      </AntForm>
    )
  }
}
