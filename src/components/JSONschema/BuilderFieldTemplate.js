import React from 'react'
import classnames from 'classnames'
import { Form, Button, Popconfirm, Menu, Icon, Dropdown } from 'antd'
import { getUiOptions } from 'react-jsonschema-form/lib/utils'

function BuilderFieldTemplate (props) {
  const {
    id,
    label,
    children,
    errors,
    help,
    description,
    hidden,
    required,
    displayLabel,
    formContext,
    classNames,
    rawErrors,
    rawHelp,
    uiSchema,
    schema,
    ...otherProps
  } = props

  if (hidden) {
    return children
  }

  const { labelCol, wrapperCol } = getUiOptions(uiSchema)
  let className = classNames.replace('form-group', '').trim()
  const { builder, order } = formContext

  const formGroupProps = {
    id,
    required,
    className: classnames(className, 'builder-field'),
    help: rawHelp,
    label: displayLabel ? label : false,
    labelCol: { span: labelCol || 6 },
    wrapperCol: { span: wrapperCol || 14 }
  }

  if (rawErrors && rawErrors.length) {
    formGroupProps.validateStatus = 'error'
    formGroupProps.help = rawErrors[0]
    formGroupProps.hasFeedback = schema.type !== 'boolean'
  }

  if (id === 'root') {
    return (
      <div id={id} className={classnames('field', 'field-object')}>
        {children}
      </div>
    )
  }

  const prop = id.split('_')[1]

  const upButton = (
    <Menu.Item onClick={() => builder.moveField('up', prop)} key='up'>
      <Icon size='large' type='up' /> Move Up
    </Menu.Item>
  )
  const downButton = (
    <Menu.Item onClick={() => builder.moveField('down', prop)} key='down'>
      <Icon size='large' type='down' /> Move Down
    </Menu.Item>
  )

  const menu = (
    <Menu>
      {order.length > 0 && [0] !== prop ? upButton : null}
      {order.length > 0 && order[order.length - 1] !== prop ? downButton : null}
      <Menu.Divider />
      <Menu.Item key='delete' onClick={() => builder.removeField(prop)}>
        <Icon
          size='large'
          type='delete'
          twoToneColor='#f5222d'
          theme='twoTone'
        />{' '}
        Delete
      </Menu.Item>
    </Menu>
  )

  return (
    <Form.Item {...formGroupProps}>
      {children}
      <div className='form-builder-buttons'>
        <Dropdown.Button
          style={builder.preview ? { display: 'none' } : {}}
          trigger={['click']}
          size='small'
          overlay={menu}
          onClick={() => builder.selectField(prop)}
          type={builder.selectedField === prop ? 'primary' : 'ghost'}
        >
          <Icon type='form' />
        </Dropdown.Button>
      </div>
    </Form.Item>
  )
}

export default BuilderFieldTemplate
