import React from 'react'
import { Alert } from 'antd'

export default function ErrorList (props) {
  const { errors } = props
  const items = (
    <ul>
      {errors.map((error, i) => {
        return (
          <li key={i}>
            <span className='error'>{error.stack}</span>
          </li>
        )
      })}
    </ul>
  )
  return <Alert error title='Form Error' description={items} />
}
