import React from 'react'
import { Row } from 'antd'

function ObjectFieldTemplate ({
  TitleField,
  properties,
  title,
  description,
  id
}) {
  return (
    <div>
      {id !== 'root' ? <TitleField title={title} /> : null}
      {description}
      {properties.map(({ content }) => content)}
    </div>
  )
}

export default ObjectFieldTemplate
