import React from 'react'
import classNames from 'classnames'
import css from './Tabbed.css'

export default function TabContainer (props) {
  return (
    <div className={classNames(css.TabContainer, props.className)}>
      {props.children}
    </div>
  )
}
