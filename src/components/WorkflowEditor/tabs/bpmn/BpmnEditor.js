import React, { Component } from 'react'
import { Button, Dropdown, Icon as SUIIcon } from 'semantic-ui-react'

import { Fill } from '../../slot-fill'

import { Icon, Loader } from '../../primitives'

import { WithCache, WithCachedState, CachedComponent } from '../../cached'

import PropertiesContainer from '../PropertiesContainer'

import CamundaBpmnModeler from './modeler'
import CliModule from 'bpmn-js-cli'
import ModelingDslModule from 'bpmn-js-cli-modeling-dsl'

import { active as isInputActive } from '../../util/dom/is-input'

import { getBpmnEditMenu } from './getBpmnEditMenu'

import css from './BpmnEditor.css'

import generateImage from '../../util/generateImage'
const label = { circular: true, empty: true }
const COLORS = [
  {
    title: 'White',
    label,
    fill: 'white',
    stroke: 'black'
  },
  {
    title: 'Blue',
    label: { ...label, color: 'blue' },
    fill: 'rgb(189, 221, 245)',
    stroke: 'rgb(33,133,208)'
  },
  {
    title: 'Orange',
    label: { ...label, color: 'orange' },
    fill: 'rgb(255, 209, 179)',
    stroke: 'rgb(242,98,1)'
  },
  {
    title: 'Green',
    label: { ...label, color: 'green' },
    fill: 'rgb(190, 243, 203)',
    stroke: 'rgb(33,186,69)'
  },
  {
    title: 'Red',
    label: { ...label, color: 'red' },
    fill: 'rgb(246, 189, 187)',
    stroke: 'rgb(229, 57, 53)'
  },
  {
    title: 'Purple',
    label: { ...label, color: 'purple' },
    fill: 'rgb(228, 194, 239)',
    stroke: 'rgb(163,51,200)'
  },
  {
    title: 'Yellow',
    label: { ...label, color: 'yellow' },
    fill: 'rgb(253, 235, 180)',
    stroke: 'rgb(250, 189, 7)'
  },
  {
    title: 'Teal',
    label: { ...label, color: 'teal' },
    fill: 'rgb(179, 255, 251)',
    stroke: 'rgb(0,181,172)'
  },
  {
    title: 'Pink',
    label: { ...label, color: 'pink' },
    fill: 'rgb(245, 189, 220)',
    stroke: 'rgb(224,57,150)'
  },
  {
    title: 'Olive',
    label: { ...label, color: 'olive' },
    fill: 'rgb(239, 247, 187)',
    stroke: 'rgb(181,204,24)'
  },
  {
    title: 'Violet',
    label: { ...label, color: 'violet' },
    fill: 'rgb(209, 194, 239)',
    stroke: 'rgb(100,53,201)'
  }
]

export class BpmnEditor extends CachedComponent {
  constructor (props) {
    super(props)

    this.state = {
      scale: 1,
      canSave: true
    }

    this.ref = React.createRef()
    this.propertiesPanelRef = React.createRef()
  }

  componentDidMount () {
    const { layout } = this.props

    const { modeler } = this.getCached()

    this.listen('on')

    modeler.attachTo(this.ref.current)

    const minimap = modeler.get('minimap')

    if (layout.minimap) {
      minimap.toggle(layout.minimap && !!layout.minimap.open)
    }

    const propertiesPanel = modeler.get('propertiesPanel')

    propertiesPanel.attachTo(this.propertiesPanelRef.current)

    this.checkImport()
    this.resize()
  }

  componentWillUnmount () {
    const { modeler } = this.getCached()

    this.listen('off')

    modeler.detach()

    const propertiesPanel = modeler.get('propertiesPanel')

    propertiesPanel.detach()
  }

  listen (fn) {
    const { modeler } = this.getCached()
    ;[
      'import.done',
      'saveXML.done',
      'commandStack.changed',
      'selection.changed',
      'attach',
      'canvas'
    ].forEach(event => {
      modeler[fn](event, this.updateState)
    })

    modeler[fn]('elementTemplates.errors', this.handleElementTemplateErrors)

    modeler[fn]('error', 1500, this.handleError)

    modeler[fn]('minimap.toggle', this.handleMinimapToggle)
  }

  componentDidUpdate (previousProps) {
    const { xml } = previousProps
    if (this.props.xml !== xml) {
      this.checkImport()
    }
  }

  undo = () => {
    const { modeler } = this.getCached()
    modeler.get('commandStack').undo()
  }

  redo = () => {
    const { modeler } = this.getCached()
    modeler.get('commandStack').redo()
  }

  align = type => {
    const { modeler } = this.getCached()
    const selection = modeler.get('selection').get()
    modeler.get('alignElements').trigger(selection, type)
  }

  zoom = type => {
    const { modeler } = this.getCached()
    let { scale } = this.state
    if (type === 'in') {
      scale += 0.1
    } else if (type === 'out') {
      scale -= 0.1
    } else {
      scale = 1
    }
    modeler.get('canvas').zoom(scale)
    this.setState({
      scale
    })
  }

  handleMinimapToggle = event => {
    this.handleLayoutChange({
      minimap: {
        open: event.open
      }
    })
  }

  handleElementTemplateErrors = event => {
    const { errors } = event

    errors.forEach(error => {
      this.handleError({ error })
    })
  }

  handleError = event => {
    const { error } = event

    const { onError } = this.props

    onError(error)
  }

  updateState = event => {
    const { modeler } = this.getCached()

    const { onChanged } = this.props

    const commandStack = modeler.get('commandStack')
    const selection = modeler.get('selection')
    const canPaste = !modeler.get('clipboard').isEmpty()

    const selectionLength = selection.get().length

    const inputActive = isInputActive()

    const editMenu = getBpmnEditMenu({
      align: selectionLength > 1,
      canCopy: !!selectionLength,
      canPaste,
      canRedo: commandStack.canRedo(),
      canUndo: commandStack.canUndo(),
      distribute: selectionLength > 2,
      editLabel: !inputActive && !!selectionLength,
      find: !inputActive,
      globalConnectTool: !inputActive,
      handTool: !inputActive,
      lassoTool: !inputActive,
      moveToOrigin: !inputActive,
      spaceTool: !inputActive,
      moveCanvas: !inputActive,
      removeSelected: !!selectionLength
    })

    const newState = {
      align: selectionLength > 1,
      canExport: ['svg', 'png'],
      canSave: true,
      distribute: selectionLength > 2,
      redo: commandStack.canRedo(),
      setColor: selectionLength,
      undo: commandStack.canUndo()
    }

    if (typeof onChanged === 'function') {
      onChanged({
        ...newState,
        editMenu
      })
    }

    this.setState(newState)
  }

  checkImport () {
    const { modeler } = this.getCached()

    const { xml } = this.props

    if (xml !== modeler.lastXML) {
      modeler.lastXML = xml

      this.setState({
        loading: true
      })

      // TODO(nikku): apply default element templates to initial diagram
      modeler.importXML(xml, (err, warnings) => {
        if (warnings.length) {
          console.log('WARNINGS', warnings)
        }

        if (err) {
          return this.handleError({
            error: err
          })
        }

        this.setState({
          loading: false
        })
      })
    }
  }

  getXML () {
    const { modeler } = this.getCached()

    return new Promise((resolve, reject) => {
      // TODO(nikku): set current modeler version and name to the diagram

      modeler.saveXML({ format: true }, (err, xml) => {
        modeler.lastXML = xml

        if (err) {
          this.handleError({
            error: err
          })

          return reject(err)
        }

        return resolve(xml)
      })
    })
  }

  getDefinitions = () => {
    const { modeler } = this.getCached()
    return new Promise((resolve, reject) => {
      try {
        const defs = modeler.getDefinitions()
        return resolve(defs)
      } catch (e) {
        return reject(e)
      }
    })
  }

  triggerAction = (action, context) => {
    const { modeler } = this.getCached()

    if (action === 'resize') {
      return this.resize()
    }

    // TODO(nikku): handle all editor actions
    modeler.get('editorActions').trigger(action, context)
  }

  handleSetColor = (fill, stroke) => {
    this.triggerAction('setColor', {
      fill,
      stroke
    })
  }

  handleDistributeElements = type => {
    this.triggerAction('distributeElements', {
      type
    })
  }

  handleContextMenu = event => {
    const { onContextMenu } = this.props

    if (typeof onContextMenu === 'function') {
      onContextMenu(event)
    }
  }

  handleLayoutChange (newLayout) {
    const { onLayoutChanged } = this.props

    if (typeof onLayoutChanged === 'function') {
      onLayoutChanged(newLayout)
    }
  }

  resize = () => {
    const { modeler } = this.getCached()

    const canvas = modeler.get('canvas')

    canvas.resized()
  }

  render () {
    const { layout, onLayoutChanged } = this.props

    const { loading } = this.state

    return (
      <div className={css.BpmnEditor}>
        <Loader hidden={!loading} />

        <Fill name='toolbar' group='color'>
          <Dropdown
            icon={null}
            inline
            trigger={
              <Button
                size='small'
                content={<SUIIcon size='small' fitted name='dropdown' />}
                icon='paint brush'
              />
            }
            disabled={!this.state.setColor}
            pointing='top right'
          >
            <Dropdown.Menu>
              <Dropdown.Header>Set Element Color</Dropdown.Header>
              <Dropdown.Divider />
              {COLORS.map(({ fill, stroke, title, label }, key) => (
                <Dropdown.Item
                  key={key}
                  onClick={() => this.handleSetColor(fill, stroke)}
                  label={label}
                  text={title}
                />
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </Fill>

        <Fill name='toolbar' group='zoom'>
          <Button.Group icon size='small'>
            <Button
              title='Zoom in'
              onClick={() => this.zoom('in')}
              icon='zoom in'
            />
            <Button
              title='Zoom out center'
              onClick={() => this.zoom('out')}
              icon='zoom out'
            />
            <Button
              title='Zoom reset'
              onClick={() => this.zoom('reset')}
              icon='crosshairs'
            />
          </Button.Group>
        </Fill>

        <Fill name='toolbar' group='align'>
          <Button.Group icon size='small'>
            <Button
              title='Align elements left'
              disabled={!this.state.align}
              onClick={() => this.align('left')}
            >
              <Icon name='align-left-tool' />
            </Button>
            <Button
              title='Align elements center'
              disabled={!this.state.align}
              onClick={() => this.align('center')}
            >
              <Icon name='align-center-tool' />
            </Button>
            <Button
              title='Align elements right'
              disabled={!this.state.align}
              onClick={() => this.align('right')}
            >
              <Icon name='align-right-tool' />
            </Button>
            <Button
              title='Align elements top'
              disabled={!this.state.align}
              onClick={() => this.align('top')}
            >
              <Icon name='align-top-tool' />
            </Button>
            <Button
              title='Align elements middle'
              disabled={!this.state.align}
              onClick={() => this.align('middle')}
            >
              <Icon name='align-middle-tool' />
            </Button>
            <Button
              title='Align elements bottom'
              disabled={!this.state.align}
              onClick={() => this.align('bottom')}
            >
              <Icon name='align-bottom-tool' />
            </Button>
          </Button.Group>
        </Fill>

        <Fill name='toolbar' group='distribute'>
          <Button.Group icon size='small'>
            <Button
              title='Distribute elements horizontally'
              disabled={!this.state.distribute}
              onClick={() => this.handleDistributeElements('horizontal')}
            >
              <Icon name='distribute-horizontal-tool' />
            </Button>
            <Button
              title='Distribute elements vertically'
              disabled={!this.state.distribute}
              onClick={() => this.handleDistributeElements('vertical')}
            >
              <Icon name='distribute-vertical-tool' />
            </Button>
          </Button.Group>
        </Fill>

        <div
          className='diagram'
          ref={this.ref}
          onFocus={this.updateState}
          onContextMenu={this.handleContextMenu}
        />

        <PropertiesContainer
          className='properties'
          layout={layout}
          ref={this.propertiesPanelRef}
          onLayoutChanged={onLayoutChanged}
        />
      </div>
    )
  }

  static createCachedState () {
    // TODO(nikku): wire element template loading
    const modeler = new CamundaBpmnModeler({
      position: 'absolute',
      additionalModules: [ModelingDslModule],
      cli: {
        bindTo: 'cli'
      }
    })

    return {
      modeler,
      __destroy: () => {
        modeler.destroy()
      }
    }
  }
}

export default WithCache(WithCachedState(BpmnEditor))

class Color extends Component {
  render () {
    const { fill, onClick, stroke, title, ...rest } = this.props

    return (
      <div
        className={css.Color}
        onClick={onClick}
        style={{
          backgroundColor: fill,
          borderColor: stroke
        }}
        title={title}
        {...rest}
      />
    )
  }
}
