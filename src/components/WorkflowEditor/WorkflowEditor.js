import React, { Component } from 'react'

import { WithCache } from './cached'

import { Fill, SlotFillRoot } from './slot-fill'

import { debounce } from 'min-dash'
import pDefer from 'p-defer'

import Toolbar from './Toolbar'
import EmptyTab from './EmptyTab'

import {
  DropdownButton,
  TabLinks,
  TabContainer,
  Tab,
  Icon,
  Loader
} from './primitives'

import { Button } from 'semantic-ui-react'

import History from './History'

import css from './WorkflowEditor.css'

const tabLoaded = {
  empty: EmptyTab
}

export const EMPTY_TAB = {
  id: '__empty',
  type: 'empty'
}

const INITIAL_STATE = {
  activeTab: EMPTY_TAB,
  Tab: EmptyTab,
  dirtyTabs: {},
  layout: {},
  tabs: [],
  tabState: {},
  logEntries: []
}

export class WorkflowEditor extends Component {
  constructor (props, context) {
    super()

    this.state = {
      ...INITIAL_STATE,
      tabShown: pDefer()
    }

    // TODO(nikku): make state
    this.tabHistory = new History()

    // TODO(nikku): make state
    this.closedTabs = new History()

    this.tabRef = React.createRef()
  }

  createDiagram = async initialTab => {
    const tab = this.addTab(initialTab)

    await this.showTab(tab)

    return tab
  }

  /**
   * Add a tab to the tab list.
   */
  addTab (tab) {
    this.setState(state => {
      const { tabs, activeTab } = state

      if (tabs.indexOf(tab) !== -1) {
        throw new Error('tab exists')
      }

      const insertIdx = tabs.indexOf(activeTab) + 1

      return {
        tabs: [...tabs.slice(0, insertIdx), tab, ...tabs.slice(insertIdx)]
      }
    })

    return tab
  }

  /**
   * Show the tab.
   *
   * @param {Tab} tab
   */
  showTab = tab => {
    const { activeTab, tabShown } = this.state

    if (tab === activeTab) {
      return tabShown.promise
    }

    const tabHistory = this.tabHistory

    tabHistory.push(tab)

    return this.setActiveTab(tab)
  }

  setActiveTab (tab) {
    const { activeTab, tabShown } = this.state

    if (activeTab === tab) {
      return tabShown.promise
    }

    if (tab !== EMPTY_TAB) {
      const navigationHistory = this.tabHistory

      if (navigationHistory.get() !== tab) {
        navigationHistory.push(tab)
      }
    }

    const deferred = pDefer()

    this.setState({
      activeTab: tab,
      Tab: this.loadTab(tab),
      tabShown: deferred,
      tabState: {},
      tabLoadingState: 'loading'
    })

    return deferred.promise
  }

  isDirty = tab => {
    return isNew(tab) || this.state.dirtyTabs[tab.id]
  }

  handleLayoutChanged = newLayout => {
    const { layout } = this.state

    this.setLayout({
      ...layout,
      ...newLayout
    })
  }

  /**
   * Mark a tab as shown.
   *
   * @param {Object} tab descriptor
   *
   * @return {Function} tab shown callback
   */
  handleTabShown = tab => () => {
    const { openedTabs, activeTab, tabShown } = this.state

    if (tab === activeTab) {
      tabShown.resolve()
    } else {
      tabShown.reject(new Error('tab miss-match'))
    }

    this.setState({
      openedTabs: {
        ...openedTabs,
        [activeTab.id]: true
      },
      tabLoadingState: 'shown'
    })
  }

  /**
   * Handle tab error.
   *
   * @param {Object} tab descriptor
   *
   * @return {Function} tab error callback
   */
  handleTabError = tab => error => {
    this.handleError(error, tab)
  }

  /**
   * Handle tab changed.
   *
   * @param {Object} tab descriptor
   *
   * @return {Function} tab changed callback
   */
  handleTabChanged = tab => (properties = {}) => {
    let { dirtyTabs, tabState } = this.state

    if ('dirty' in properties) {
      const newDirtyTabs = {
        ...dirtyTabs,
        [tab.id]: properties.dirty
      }

      this.setState({
        dirtyTabs: newDirtyTabs
      })
    }

    tabState = {
      ...tabState,
      ...properties
    }

    this.setState({
      tabState
    })

    this.updateMenu(tabState)
  }

  async saveTab (tab, options = {}) {
    await this.showTab(tab)
    const contents = await this.tabRef.current.triggerAction('save')
    this.handleSave(contents)
  }

  tabSaved (tab, newFile) {
    const { dirtyTabs, tabs } = this.state

    tab.file = newFile

    this.setState({
      tabs: [...tabs],
      dirtyTabs: {
        ...dirtyTabs,
        [tab.id]: false
      }
    })
  }

  loadTab (tab) {
    const type = tab.type

    if (tabLoaded[type]) {
      return tabLoaded[type]
    }

    const { tabsProvider } = this.props

    var tabComponent =
      tabsProvider.getTabComponent(type) || missingProvider(type)

    Promise.resolve(tabComponent).then(c => {
      var Tab = c.default || c

      tabLoaded[type] = Tab

      if (this.state.activeTab === tab) {
        this.setState({
          Tab
        })
      }
    })

    return LoadingTab
  }

  componentDidMount () {
    const { onReady } = this.props

    if (typeof onReady === 'function') {
      onReady().then(initialTab => {
        this.createDiagram(initialTab)
      })
    }
  }

  componentDidUpdate (prevProps, prevState) {
    const { activeTab, tabs, tabLoadingState, layout } = this.state

    const { onTabChanged, onTabShown } = this.props

    if (prevState.activeTab !== activeTab) {
      if (typeof onTabChanged === 'function') {
        onTabChanged(activeTab, prevState.activeTab)
      }
    }

    if (tabLoadingState === 'shown' && prevState.tabLoadingState !== 'shown') {
      if (typeof onTabShown === 'function') {
        onTabShown(activeTab)
      }
    }
  }

  componentDidCatch (error, info) {
    this.handleError(error)
  }

  handleError (error, ...args) {
    const { onError } = this.props

    if (typeof onError === 'function') {
      onError(error, ...args)
    }

    // TODO: show error in log
  }

  setLayout (layout) {
    this.setState({
      layout
    })
  }

  handleSave = contents => {
    this.props.onSave(contents)
  }

  handlePublish = () => {
    this.props.onPublish()
  }

  handleUnpublish = () => {
    this.props.onUnpublish()
  }

  handleDuplicate = () => {
    this.props.onDuplicate()
  }

  clearLog = () => {
    this.setState({
      logEntries: []
    })
  }

  toggleLog = open => {
    this.handleLayoutChanged({
      log: { open }
    })
  }

  updateMenu = state => {
    return state
  }

  triggerAction = (action, options) => {
    const { activeTab } = this.state

    console.log('WorkflowEditor#triggerAction %s %o', action, options)

    if (action === 'create-bpmn-diagram') {
      return this.createDiagram('bpmn')
    }

    if (action === 'save') {
      return this.saveTab(activeTab)
    }

    if (action === 'update-menu') {
      return this.updateMenu()
    }

    if (action === 'publish') {
      return this.handlePublish()
    }

    if (action === 'unpublish') {
      return this.handleUnpublish()
    }

    if (action === 'duplicate') {
      return this.handleDuplicate()
    }

    const tab = this.tabRef.current

    return tab.triggerAction(action, options)
  }

  quit () {
    return true
  }

  composeAction = (...args) => async event => {
    await this.triggerAction(...args)
  }

  render () {
    const {
      tabs,
      activeTab,
      tabState,
      layout,
      logEntries,
      canSave
    } = this.state

    const { onPublish, onUnpublish, onSave, openDrawer } = this.props
    const Tab = this.state.Tab

    return (
      <div className={css.WorkflowEditor}>
        <SlotFillRoot>
          <Toolbar />

          <Fill name='toolbar' group='save'>
            <Button.Group icon size='small'>
              <Button
                primary
                icon='setting'
                title='Open Settings'
                onClick={() => openDrawer()}
              />
            </Button.Group>
            <span className={'separator'} />
            <Button.Group icon size='small'>
              <Button
                disabled={
                  !this.isDirty(activeTab) || !tabState.canSave || !onSave
                }
                onClick={this.composeAction('save')}
                icon='save'
                title='Save diagram'
              />
              <Button
                icon='upload'
                title='Publish'
                disabled={!onPublish}
                onClick={this.composeAction('publish')}
              />
              <Button
                icon='stop circle'
                title='Unpublish'
                disabled={!onUnpublish}
                onClick={this.composeAction('unpublish')}
              />
            </Button.Group>
          </Fill>

          <Fill name='toolbar' group='editor'>
            <Button.Group icon size='small'>
              <Button
                disabled={!tabState.undo}
                onClick={this.composeAction('undo')}
                title='Undo last action'
                icon='undo'
              />
              <Button
                disabled={!tabState.redo}
                onClick={this.composeAction('redo')}
                title='Redo last action'
                icon='redo'
              />
            </Button.Group>
          </Fill>

          <div className='tabs'>
            <TabContainer className='main'>
              {Tab === EmptyTab ? (
                <EmptyTab
                  key={activeTab.id}
                  tab={activeTab}
                  onShown={this.handleTabShown(activeTab)}
                  onAction={this.triggerAction}
                  ref={this.tabRef}
                />
              ) : (
                <Tab
                  key={activeTab.id}
                  tab={activeTab}
                  layout={layout}
                  onChanged={this.handleTabChanged(activeTab)}
                  onError={this.handleTabError(activeTab)}
                  onShown={this.handleTabShown(activeTab)}
                  onLayoutChanged={this.handleLayoutChanged}
                  onContextMenu={this.openTabMenu}
                  ref={this.tabRef}
                />
              )}
            </TabContainer>
          </div>
        </SlotFillRoot>
      </div>
    )
  }
}

function missingProvider (providerType) {
  class MissingProviderTab extends Component {
    componentDidMount () {
      this.props.onShown()
    }

    render () {
      return (
        <Tab key='missing-provider'>
          <span>Cannot open tab: no provider for {providerType}.</span>
        </Tab>
      )
    }
  }

  return MissingProviderTab
}

class LoadingTab extends Component {
  render () {
    return (
      <Tab key='loading'>
        <Loader />
      </Tab>
    )
  }
}

function isNew (tab) {
  return tab.file && !tab.file.path
}

export default WithCache(WorkflowEditor)
