import React from 'react'
import { Button, Popup } from 'semantic-ui-react'

const EditingTools = ({
  onUndo,
  onRedo,
  onZoomIn,
  onZoomOut,
  onZoomReset,
  onSave,
  onPublish,
  onUnpublish,
  onDuplicate
}) => {
  const publishBtn = (
    <Popup
      trigger={
        <Button icon='upload' content='Publish' onClick={onPublish || null} />
      }
      content='Publish'
      inverted
      size='mini'
      position='bottom center'
    />
  )
  const unpublishBtn = (
    <Popup
      trigger={
        <Button
          icon='stop circle'
          content='Unpublish'
          onClick={onUnpublish || null}
        />
      }
      content='Publish'
      inverted
      size='mini'
      position='bottom center'
    />
  )
  const duplicateBtn = (
    <Popup
      trigger={<Button icon='clone' onClick={onDuplicate || null} />}
      content='Duplicate Workflow'
      inverted
      size='mini'
      position='bottom center'
    />
  )
  return (
    <div className='io-editing-tools' style={{ display: 'block', right: 300 }}>
      <Button.Group>
        <Popup
          trigger={<Button icon='undo' onClick={onUndo} />}
          content='Undo Changes'
          inverted
          size='mini'
          position='bottom center'
        />
        <Popup
          trigger={<Button icon='redo' onClick={onRedo} />}
          content='Redo Changes'
          inverted
          size='mini'
          position='bottom center'
        />
      </Button.Group>{' '}
      <Button.Group>
        <Popup
          trigger={<Button icon='zoom in' onClick={onZoomIn} />}
          content='Zoom In'
          inverted
          size='mini'
          position='bottom center'
        />
        <Popup
          trigger={<Button icon='zoom out' onClick={onZoomOut} />}
          content='Zoom Out'
          inverted
          size='mini'
          position='bottom center'
        />
        <Popup
          trigger={<Button icon='crosshairs' onClick={onZoomReset} />}
          content='Zoom Reset'
          inverted
          size='mini'
          position='bottom center'
        />
      </Button.Group>{' '}
      <Button.Group>
        <Popup
          trigger={<Button icon='save' onClick={onSave} />}
          content='Save Workflow'
          inverted
          size='mini'
          position='bottom center'
        />
        {onDuplicate ? duplicateBtn : null}
        {onPublish ? publishBtn : null}
        {onUnpublish ? unpublishBtn : null}
      </Button.Group>
    </div>
  )
}

export default EditingTools
