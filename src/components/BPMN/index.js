import React, { Component, Fragment } from 'react'
import propTypes from 'prop-types'
import BpmnModeler from 'bpmn-js/lib/Modeler'
import propertiesPanelModule from 'bpmn-js-properties-panel'
import propertiesProviderModule from 'bpmn-js-properties-panel/lib/provider/camunda'
import camundaModdleDescriptor from 'camunda-bpmn-moddle/resources/camunda'
import { Tab } from 'semantic-ui-react'

import { WithCache } from 'components/Cached'
import XMLEditor from 'components/Editor/XML'
import EditingTools from './components/EditingTools'

let scale = 1

class BPMN extends Component {
  static propTypes = {
    published: propTypes.bool,
    canPublish: propTypes.bool,
    canDuplicate: propTypes.bool,
    xml: propTypes.string,
    onSave: propTypes.func,
    doPublish: propTypes.func,
    doUnpublish: propTypes.func,
    doDuplicate: propTypes.func,
    onChangeXml: propTypes.func
  }

  componentDidMount () {
    document.body.className = 'shown'

    this.bpmnModeler = new BpmnModeler({
      additionalModules: [propertiesPanelModule, propertiesProviderModule],
      container: '#canvas',
      propertiesPanel: {
        parent: '#properties-panel'
      }
    })

    const { xml } = this.props

    this.renderDiagram(xml)
  }

  componentWillReceiveProps (nextProps) {
    const { xml } = nextProps
    if (xml && xml !== this.props.xml) {
      this.renderDiagram(xml)
    }
  }

  shouldComponentUpdate () {
    return false
  }

  renderDiagram = xml => {
    this.bpmnModeler.importXML(xml, err => {
      if (err) {
        console.log('error rendering', err)
      } else {
        this.props.onChangeXml(xml)
        this.bpmnModeler.getDefinitions()
        console.log('successfully rendered')
      }
    })
  }

  handleSave = e => {
    const { onSave } = this.props

    this.bpmnModeler.saveXML({ format: true }, (err, xml) => {
      if (err) {
        console.error(err)
      }
      const bpmnDefs = this.bpmnModeler.getDefinitions()
      const { name, id } = bpmnDefs.rootElements[0]
      onSave(xml, name, id)
    })
  }

  handleRedo = () => {
    this.bpmnModeler.get('commandStack').redo()
  }

  handleUndo = () => {
    this.bpmnModeler.get('commandStack').undo()
  }

  handleZoom = () => {
    this.bpmnModeler.get('canvas').zoom(scale)
  }

  handleZoomIn = () => {
    scale += 0.1
    this.handleZoom()
  }

  handleZoomOut = () => {
    if (scale <= 0.3) {
      scale = 0.2
    } else {
      scale -= 0.1
    }
    this.handleZoom()
  }

  handleZoomReset = () => {
    scale = 1
    this.handleZoom()
  }

  handlePublish = () => {
    this.props.doPublish()
  }

  handleUnpublish = () => {
    this.props.doUnpublish()
  }

  handleDuplicate = () => {
    this.props.doDuplicate()
  }

  renderXMLView = () => {
    const { xml, onChangeXml } = this.props
    if (this.bpmnModeler) {
      this.bpmnModeler.saveXML({ format: true }, (err, xmlStr) => {
        if (err) {
          console.error(err)
        }
        this.onEditXml(xmlStr)
      })
    }

    return <XMLEditor code={xml} onChange={onChangeXml} />
  }

  renderDiagramView = () => {
    const { published, canPublish, canDuplicate } = this.props
    return (
      <div id='bpmn-diagram'>
        <div className='content'>
          <div id='canvas' />
          <div id='properties-panel' />
        </div>
        <EditingTools
          onSave={this.handleSave}
          onRedo={this.handleRedo}
          onUndo={this.handleUndo}
          onSaveFile={this.handleSaveFile}
          onZoomIn={this.handleZoomIn}
          onZoomOut={this.handleZoomOut}
          onZoomReset={this.handleZoomReset}
          onPublish={canPublish && !published ? this.handlePublish : false}
          onUnpublish={published && !canPublish ? this.handleUnpublish : false}
          onDuplicate={canDuplicate ? this.handleDuplicate : false}
        />
      </div>
    )
  }

  render () {
    const { published, canPublish, canDuplicate } = this.props
    const style = { height: '100%' }
    const panes = [
      {
        menuItem: 'Builder',
        pane: { style, key: 'builder', content: 'This is builder mode' }
      },
      {
        menuItem: 'Diagram',
        pane: { style, key: 'diagram', content: this.renderDiagramView() }
      },
      {
        menuItem: 'XML',
        pane: { style, key: 'xml', content: this.renderXMLView() }
      }
    ]

    return (
      <div style={{ ...style }}>
        <Tab style={{ ...style }} panes={panes} renderActiveOnly={false} />
      </div>
    )
  }
}

export default WithCache(BPMN)
