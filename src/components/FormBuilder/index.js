import React from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Input,
  InputNumber,
  Row,
  Col,
  Card,
  Select,
  Empty,
  Switch,
  Button,
  Icon
} from 'antd'
import _ from 'lodash'
import shortid from 'shortid'
import { fromJS } from 'immutable'
import {
  Form as JSONForm,
  ObjectFieldBuilderTemplate,
  BuilderFieldTemplate
} from 'components/JSONschema'
import { toastr } from 'react-redux-toastr'
import styles from './formBuilder.module.scss'

class FormBuilder extends React.Component {
  static propTypes = {
    data: PropTypes.object,
    toggleView: PropTypes.string,
    setImmState: PropTypes.func
  }

  state = {
    formData: {}
  }

  setImmState = fn => {
    this.props.setImmState(fn)
  }

  updateMeta = val => {
    this.setImmState(s => s.setIn(['meta', 'listFields'], fromJS(val)))
  }

  moveField = (dir, id) => {
    this.setImmState(s => {
      let order = s.getIn(['uiSchema', 'ui:order'])
      const from = order.indexOf(id)
      const to = dir === 'up' ? from - 1 : from + 0
      const po = dir === 'down' ? from + 0 : from - 1
      const target = order.get(to)
      const target2 = order.get(po)
      order = order.delete(from)
      const targetIndex = order.indexOf(target, target2)
      order = order.insert(targetIndex, id)
      return s.setIn(['uiSchema', 'ui:order'], order)
    })
  }

  editField = (value, name) => {
    const selectedField = this.props.data.selectedField
    this.setImmState(s =>
      value
        ? s.setIn(['schema', 'properties', selectedField, name], value)
        : s.deleteIn(['schema', 'properties', selectedField, name])
    )
  }

  switchFieldType = value => {
    const selectedField = this.props.data.selectedField
    const field = this.props.data.getIn(['schema', 'properties', selectedField])
    this.setImmState(s => {
      let newField = fromJS({ type: value, title: field.get('title') })
      let newFieldUi =
        value === 'boolean'
          ? fromJS({
            'ui:widget': 'checkbox',
            'ui:label': 'Label for checkbox'
          })
          : fromJS({ 'ui:placeholder': '' })

      return s
        .setIn(['schema', 'properties', selectedField], newField)
        .setIn(['uiSchema', selectedField], newFieldUi)
    })
  }

  editFieldUi = (value, name) => {
    const selectedField = this.props.data.selectedField
    this.setImmState(s => s.setIn(['uiSchema', selectedField, name], value))
  }

  togglePropertyKey = (checked, name, type) => {
    const selectedField = this.props.data.selectedField
    let value
    switch (type) {
      case 'string':
        value = checked ? '' : undefined
        break
      case 'number':
        value = checked ? 1 : undefined
        break
      case 'boolean':
        value = checked ? true : undefined
        break
    }
    this.editField(value, name)
  }

  toggleRequired = key => {
    this.setImmState(s => {
      let req = s.getIn(['schema', 'required'])
      req = req.contains(key) ? req.filter(f => f !== key) : req.push(key)
      return s.setIn(['schema', 'required'], req)
    })
  }

  toggleMultiple = checked => {
    const selectedField = this.props.data.selectedField
    this.setImmState(s => {
      const selUi = s.getIn(['uiSchema', selectedField])
      if (checked) {
        s = s.setIn(['schema', 'properties', selectedField, 'multiple'], true)
        return selUi.has('ui:options')
          ? s.setIn(
            ['uiSchema', selectedField, 'ui:options', 'mode'],
            'multiple'
          )
          : s.setIn(
            ['uiSchema', selectedField, 'ui:options'],
            fromJS({ mode: 'multiple' })
          )
        return s
      } else {
        s = s.deleteIn(['schema', 'properties', selectedField, 'multiple'])
        return selUi.get('ui:options').count() > 1
          ? s.deleteIn(['uiSchema', selectedField, 'ui:options', 'mode'])
          : s.deleteIn(['uiSchema', selectedField, 'ui:options'])
      }
    })
  }

  toggleEnum = checked => {
    const selectedField = this.props.data.selectedField
    this.setImmState(s => {
      if (checked) {
        return s
          .setIn(['schema', 'properties', selectedField, 'enum'], fromJS(['']))
          .setIn(
            ['schema', 'properties', selectedField, 'enumNames'],
            fromJS([''])
          )
      } else {
        return s
          .deleteIn(['schema', 'properties', selectedField, 'enum'])
          .deleteIn(['schema', 'properties', selectedField, 'enumNames'])
      }
    })
  }

  updateEnum = (val, index, label = false) => {
    const selectedField = this.props.data.selectedField
    const key = label ? 'enumNames' : 'enum'
    this.setImmState(s =>
      s.setIn(['schema', 'properties', selectedField, key, index], val)
    )
  }

  addOption = () => {
    const selectedField = this.props.data.selectedField
    this.setImmState(s => {
      const ops = s.getIn(['schema', 'properties', selectedField, 'enum'])
      const labels = s.getIn([
        'schema',
        'properties',
        selectedField,
        'enumNames'
      ])
      return s
        .setIn(['schema', 'properties', selectedField, 'enum'], ops.push(''))
        .setIn(
          ['schema', 'properties', selectedField, 'enumNames'],
          labels.push('')
        )
    })
  }

  removeOption = index => {
    const selectedField = this.props.data.selectedField
    this.setImmState(s => {
      return s
        .deleteIn(['schema', 'properties', selectedField, 'enum', index])
        .deleteIn(['schema', 'properties', selectedField, 'enumNames', index])
    })
  }

  addField = ({ key }) => {
    const newId = shortid.generate()
    const newField = fromJS({
      type: key,
      title: 'Untitled Field'
    })
    let uiEntry =
      key === 'boolean'
        ? fromJS({ 'ui:widget': 'checkbox', 'ui:label': 'Label for checkbox' })
        : fromJS({ 'ui:placeholder': '' })

    this.setImmState(s => {
      const order = s.getIn(['uiSchema', 'ui:order'])

      return s
        .setIn(['schema', 'properties', newId], newField)
        .setIn(['uiSchema', newId], uiEntry)
        .setIn(['uiSchema', 'ui:order'], order.push(newId))
        .set('selectedField', newId)
    })
  }

  removeField = id => {
    this.setImmState(s => {
      let _s = s
        .deleteIn(['schema', 'properties', id])
        .deleteIn(['uiSchema', id])
        .setIn(
          ['uiSchema', 'ui:order'],
          s.getIn(['uiSchema', 'ui:order']).filterNot(f => f === id)
        )
      return _s.get('selectedField') === id ? _s.set('selectedField', null) : _s
    })
  }

  selectField = id => {
    this.setImmState(s => s.set('selectedField', id))
  }

  renderFieldInfo = () => {
    const { data } = this.props
    const selectedField = data.get('selectedField')
    const sel = data.getIn(['schema', 'properties', selectedField])
    const req = data.getIn(['schema', 'required'])
    const selUi = data.getIn(['uiSchema', selectedField])
    const type = sel.get('type')
    let typeSpecific = null

    switch (type) {
      case 'string':
        typeSpecific = (
          <div>
            <Form.Item
              label='Format'
              style={{ paddingBottom: 0, marginBottom: 0 }}
            >
              <Select
                allowClear
                onChange={v => this.editField(v, 'format')}
                value={sel.get('format')}
                placeholder='Select a format type'
              >
                <Select.Option value='email'>E-mail</Select.Option>
                <Select.Option value='uri'>URL</Select.Option>
                <Select.Option value='date-time'>Date Time</Select.Option>
                <Select.Option value='date'>Date</Select.Option>
              </Select>
            </Form.Item>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  label='Min Length'
                  style={{ paddingBottom: 0, marginBottom: 0 }}
                >
                  <Row>
                    <Col span={10}>
                      <Switch
                        checked={sel.has('minLength')}
                        onChange={c =>
                          this.togglePropertyKey(c, 'minLength', 'number')
                        }
                      />
                    </Col>
                    <Col span={10}>
                      <InputNumber
                        disabled={!sel.has('minLength')}
                        onChange={v => this.editField(v, 'minLength')}
                        value={sel.get('minLength')}
                      />
                    </Col>
                  </Row>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  label='Max Length'
                  style={{ paddingBottom: 0, marginBottom: 0 }}
                >
                  <Row>
                    <Col span={10}>
                      <Switch
                        checked={sel.has('maxLength')}
                        onChange={c =>
                          this.togglePropertyKey(c, 'maxLength', 'number')
                        }
                      />
                    </Col>
                    <Col span={10}>
                      <InputNumber
                        disabled={!sel.has('maxLength')}
                        onChange={v => this.editField(v, 'maxLength')}
                        value={sel.get('maxLength')}
                      />
                    </Col>
                  </Row>
                </Form.Item>
              </Col>
            </Row>
          </div>
        )
        break
      case 'number':
        typeSpecific = (
          <Row gutter={24}>
            <Col span={12}>
              <Form.Item
                label='Minimum'
                style={{ paddingBottom: 0, marginBottom: 0 }}
              >
                <Row>
                  <Col span={10}>
                    <Switch
                      checked={sel.has('minimum')}
                      onChange={c =>
                        this.togglePropertyKey(c, 'minimum', 'number')
                      }
                    />
                  </Col>
                  <Col span={10}>
                    <InputNumber
                      disabled={!sel.has('minimum')}
                      onChange={v => this.editField(v, 'minimum')}
                      value={sel.get('minimum')}
                    />
                  </Col>
                </Row>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label='Maximum'
                style={{ paddingBottom: 0, marginBottom: 0 }}
              >
                <Row>
                  <Col span={10}>
                    <Switch
                      checked={sel.has('maximum')}
                      onChange={c =>
                        this.togglePropertyKey(c, 'maximum', 'number')
                      }
                    />
                  </Col>
                  <Col span={10}>
                    <InputNumber
                      disabled={!sel.has('maximum')}
                      onChange={v => this.editField(v, 'maximum')}
                      value={sel.get('maximum')}
                    />
                  </Col>
                </Row>
              </Form.Item>
            </Col>
          </Row>
        )
        break
    }

    return (
      <Form>
        <Row gutter={24}>
          <Col span={14}>
            <Form.Item
              label='Title'
              style={{ paddingBottom: 0, marginBottom: 0 }}
            >
              <Input
                defaultValue='Untitled Field'
                onChange={({ target }) => this.editField(target.value, 'title')}
                value={sel.get('title')}
              />
            </Form.Item>
            {typeSpecific}
          </Col>
          <Col span={10}>
            <Form.Item
              label='Field Type'
              style={{ paddingBottom: 0, marginBottom: 0 }}
            >
              <Select
                onChange={v => this.switchFieldType(v)}
                value={sel.get('type')}
                placeholder='Select a field type'
              >
                <Select.Option value='string'>Text</Select.Option>
                <Select.Option value='number'>Number</Select.Option>
                <Select.Option value='boolean'>Boolean</Select.Option>
              </Select>
            </Form.Item>
            {sel.get('type') === 'boolean' ? (
              <Form.Item
                label='Label'
                style={{ paddingBottom: 0, marginBottom: 0 }}
              >
                <Input
                  onChange={({ target }) =>
                    this.editFieldUi(target.value, 'ui:label')
                  }
                  value={selUi.get('ui:label')}
                />
              </Form.Item>
            ) : (
              <Form.Item
                label='Placeholder'
                style={{ paddingBottom: 0, marginBottom: 0 }}
              >
                <Input
                  onChange={({ target }) =>
                    this.editFieldUi(target.value, 'ui:placeholder')
                  }
                  value={selUi.get('ui:placeholder')}
                />
              </Form.Item>
            )}
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <Form.Item
              label='Required'
              style={{ paddingBottom: 0, marginBottom: 0 }}
            >
              <Switch
                checked={req.contains(selectedField)}
                onChange={() => this.toggleRequired(selectedField)}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label='Selection'
              style={{
                paddingBottom: 0,
                marginBottom: 0,
                display: 'inline-block'
              }}
            >
              <Switch
                checked={sel.has('enum')}
                onChange={c => this.toggleEnum(c)}
              />
            </Form.Item>
          </Col>
          {sel.has('enum') ? (
            <div>
              <Col span={8}>
                <Form.Item
                  label='Multiple'
                  style={{
                    paddingBottom: 0,
                    marginBottom: 0,
                    display: 'inline-block'
                  }}
                >
                  <Switch
                    checked={sel.get('multiple') === true}
                    onChange={c =>
                      this.toggleMultiple(c, 'multiple', 'boolean')
                    }
                  />
                </Form.Item>
              </Col>
              {sel.get('enum').map((v, k) => {
                const valInput =
                  sel.get('type') === 'string' ? (
                    <Input
                      defaultValue=''
                      onChange={({ target }) => this.updateEnum(target, k)}
                      value={v}
                    />
                  ) : (
                    <InputNumber
                      defaultValue=''
                      onChange={({ target }) => this.updateEnum(target, k)}
                      value={v}
                    />
                  )
                return (
                  <Row key={k} gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        label='Value'
                        style={{ paddingBottom: 0, marginBottom: 0 }}
                      >
                        {valInput}
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        label='Label'
                        style={{ paddingBottom: 0, marginBottom: 0 }}
                      >
                        <Row gutter={24}>
                          <Col span={10}>
                            <Input
                              defaultValue=''
                              onChange={({ target }) => {
                                console.log(target)
                                return this.updateEnum(target.value, k, true)
                              }}
                              value={sel.getIn(['enumNames', k])}
                            />
                          </Col>
                          <Col span={2}>
                            {k !== 0 ? (
                              <Button
                                type='danger'
                                size='small'
                                icon='delete'
                                onClick={() => this.removeOption(k)}
                              />
                            ) : null}
                          </Col>
                        </Row>
                      </Form.Item>
                    </Col>
                  </Row>
                )
              })}
              <Col span={24}>
                <Button
                  type='dashed'
                  block
                  onClick={() => this.addOption()}
                  icon='plus-circle'
                  size='small'
                >
                  Add Option
                </Button>
              </Col>
            </div>
          ) : null}
        </Row>
      </Form>
    )
  }

  renderOptions = () => {
    const schema = this.props.data.schema
    const listFields = this.props.data.getIn(['meta', 'listFields']).toJS()

    const listFieldOps = schema
      .get('properties')
      .map((v, k) => (
        <Select.Option key={k} value={k}>
          {v.get('title')}
        </Select.Option>
      ))
      .toList()
      .toJS()

    return (
      <Row style={{ padding: '1em' }}>
        <Col span={12}>
          <Form layout='horizontal'>
            <Form.Item label='Include as Columns in List View'>
              <Select
                mode='multiple'
                value={listFields}
                onChange={this.updateMeta}
              >
                {listFieldOps}
              </Select>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    )
  }

  renderFields () {
    const { formData } = this.state
    const { selectedField, schema, uiSchema } = this.props.data.toJS()
    const editorProps = {
      formData,
      onChange: this.updateFormData,
      schema,
      uiSchema,
      onSubmit: () => toastr.success('Form Validation Success.'),
      noHtml5Validate: true,
      showErrorList: false,
      buttonLabel: 'Test Validation',
      className: 'ui tiny form',
      ObjectFieldTemplate: ObjectFieldBuilderTemplate,
      FieldTemplate: BuilderFieldTemplate,
      formContext: {
        order: uiSchema['ui:order'],
        builder: {
          addField: this.addField,
          selectField: this.selectField,
          removeField: this.removeField,
          moveField: this.moveField,
          selectedField,
          preview: this.props.preview
        }
      }
    }

    const propsLength = Object.keys(schema.properties).length
    return (
      <Row gutter={24} style={{ padding: '1em' }}>
        <Col span={this.props.preview ? 24 : 12}>
          <Card type='inner' title='Form Preview'>
            <JSONForm {...editorProps}>
              <Row>
                <Col span={10} offset={7}>
                  <Button
                    block
                    disabled={!propsLength}
                    type='primary'
                    htmlType='submit'
                  >
                    Test Validation
                  </Button>
                </Col>
              </Row>
            </JSONForm>
          </Card>
        </Col>
        <Col
          span={12}
          className={this.props.preview ? styles['form-builder-preview'] : null}
        >
          <Card type='inner' title='Field Editor'>
            {selectedField ? (
              this.renderFieldInfo()
            ) : (
              <Empty description='Create a new field or select an existing one.' />
            )}
          </Card>
        </Col>
      </Row>
    )
  }

  render () {
    const { view, fieldsOnly } = this.props
    if (fieldsOnly) {
      return this.renderFields()
    }
    return view === 'options' ? this.renderOptions() : this.renderFields()
  }
}

FormBuilder.defaultProps = {
  view: 'fields'
}

export default FormBuilder
