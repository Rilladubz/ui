const initData = thunks => dispatch =>
  Promise.all(thunks.map(t => dispatch(t())))

export { initData }
