export const image = [
  { name: 'JPEG', id: 'image/jpeg', filenames: ['jpg', 'jpeg'] },
  { name: 'Bitmap', id: 'image/bmp', filenames: ['bmp'] },
  { name: 'TIF', id: 'image/tiff', filenames: ['tif', 'tiff'] },
  { name: 'PNG', id: 'image/png', filenames: ['png'] },
  { name: 'GIF', id: 'image/gif', filenames: ['gif'] }
]

export const audio = [
  { name: 'WAVE', id: 'audio/wave', filenames: ['wav'] },
  { name: 'OGG', id: 'audio/ogg', filenames: ['ogg'] },
  { name: 'MP3', id: 'audio/mp3', filenames: ['mp3'] }
]

export const video = [
  { name: 'MPEG Video', id: 'video/mpeg', filenames: ['mpeg'] },
  { name: 'MPEG-4', id: 'video/mp4', filenames: ['mp4'] },
  { name: 'Apple Quicktime', id: 'video/mov', filenames: ['mov'] }
]

export const text = [
  { name: 'Text Plain', id: 'text/plain', filenames: ['txt'] },
  { name: 'Rich Text', id: 'text/rtf', filenames: ['rtf'] },
  { name: 'HTML', id: 'text/html', filenames: ['htm', 'html'] }
]

export const application = [
  { name: 'Acrobat PDF', id: 'application/pdf', filenames: ['pdf'] },
  {
    name: 'Excel Spreadsheet',
    id: 'application/vnd.ms-excel',
    filenames: ['xls']
  },
  { name: 'MIME HTML File', id: 'message/rfc822', filenames: ['mhtml'] },
  { name: 'Microsoft Word', id: 'application/msword', filenames: ['doc'] },
  {
    name: 'Microsoft Word (OpenXML)',
    id:
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    filenames: ['docx']
  }
]

export const all = [...application, ...text, ...audio, ...image, ...video]

export default { audio, image, text, video, application, all }
