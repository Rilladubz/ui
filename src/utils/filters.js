import qs from 'qs'
import _ from 'lodash'

export const ops = {
  eq: { id: 'eq', title: 'equal to', symbol: '=', cam: '' },
  neq: { id: 'neq', title: 'not equal to', symbol: '!=', cam: 'NotEqual' },
  like: { id: 'like', title: 'like', symbol: 'like', cam: 'Like' },
  notlike: {
    id: 'notlike',
    title: 'not like',
    symbol: 'not like',
    cam: 'NotLike'
  }
}

export const types = {
  integer: [ops.eq, ops.neq],
  string: [ops.eq, ops.neq, ops.like],
  date: [ops.eq, ops.neq],
  boolean: [ops.eq, ops.neq]
}

export const taskTypes = {
  name: [ops.eq, ops.neq, ops.like, ops.notlike],
  assignee: [ops.eq, ops.like],
  delegationState: [ops.eq]
}

export const taskColumns = [
  { dataIndex: 'assignee', title: 'Assignee' },
  { dataIndex: 'name', title: 'Name' }
]

export function buildParams (state) {
  const params = state.get('params').toJS()
  const { sortField, sortOrder, search, searchJoin } = params

  const query = {}
  if (sortField) {
    query.orderBy = _.snakeCase(sortField)
  }
  if (sortOrder) {
    query.sortedBy = sortOrder
  }
  if (search) {
    query.search = search
      .map((v, k) => `${_.snakeCase(v.column)}:${encodeURIComponent(v.value)}`)
      .join(';')
    query.searchFields = search
      .map(v => `${_.snakeCase(v.column)}:${ops[v.operator].symbol}`)
      .join(';')
  }
  if (searchJoin) {
    query.searchJoin = searchJoin
  }

  return qs.stringify(query, { encode: false })
}

export function buildCamundaParams (state) {
  const params = state.get('params').toJS()
  const { sortField, sortOrder, search } = params

  let query = {}
  if (sortField) {
    query.sortBy = sortField
  }
  if (sortOrder) {
    query.sortOrder = sortOrder
  }
  if (search) {
    search.forEach((v, k) => {
      query[`${v.column}${ops[v.operator].cam}`] = v.value
    })
  }

  return qs.stringify(query, { encode: false })
}
