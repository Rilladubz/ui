import thunk from 'redux-thunk'
import { init } from '@rematch/core'
import loadingPlugin from '@rematch/loading'
import selectPlugin from '@rematch/select'
import subscriptionsPlugin from '@rematch/subscriptions'

import * as models from 'models'

const store = init({
  models,
  plugins: [subscriptionsPlugin(), loadingPlugin(), selectPlugin()],
  redux: {
    middleware: [thunk],
    devtoolOptions: {
      disabled: !process.env.NODE_ENV === 'development'
    }
  }
})

export default store
