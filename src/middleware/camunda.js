import axios from 'axios'

const client = axios.create({
  baseURL: process.env.REACT_APP_CAMUNDA_ENDPOINT,
  responseType: 'json'
})

client.defaults.headers.post['Content-Type'] = 'application/json'
client.defaults.headers.put['Content-Type'] = 'application/json'

export default client
