import React, { Component } from 'react'
import PropTypes from 'prop-types'

class CoreLayout extends Component {
  render () {
    const { children, user, logoutUser } = this.props
    return (
      <div className='page-layout'>
        <div className='main-content'>{children}</div>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  user: PropTypes.object.isRequired,
  children: PropTypes.node,
  logoutUser: PropTypes.func.isRequired
}

export default CoreLayout
