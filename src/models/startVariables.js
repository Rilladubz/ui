import { Record, fromJS } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import { ucFirst } from 'utils/render'
import store from './index'
import { extendFactory } from 'models/base'
import api from 'middleware/camunda'

export const StartVariables = new Record({
  id: null,
  variables: null
})

const modelConfig = {
  resourceName: {
    singular: 'StartVariable',
    plural: 'StartVariables'
  },
  schemaName: 'startVariables',
  recordType: StartVariables
}

const modelEffects = config => ({
  async fetch (id) {
    const { modelSchema } = config
    const res = await api.get(`/process-definition/key/${id}/form-variables`)
    this.merge(normalize({ variables: res.data, id }, modelSchema))
    return res.data
  },
  async fetchAll () {
    const { listSchema } = config
    const res = await api.get('/process-definition')
    this.merge(normalize(res.data, listSchema))
    return res.data
  }
})

const modelReducers = config => ({
  receiveActivities: (state, data) => {
    if (data) {
      const activities = fromJS(data)
      return state.setIn(
        ['list', `${activities.get('id')}`, 'activity'],
        activities
      )
    }
    return state
  },
  receiveVars: (state, payload) => {
    const { data, id } = payload
    const vars = fromJS(data)
    return state.setIn(['list', `${id}`, 'variables'], vars)
  }
})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
