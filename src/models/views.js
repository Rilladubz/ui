import { Record } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import { buildParams } from 'utils/filters'
import { extendFactory } from 'models/base'
import api from 'middleware/api'

export const viewSchema = new schema.Entity('views')

const View = new Record({
  id: null,
  name: null,
  userId: null,
  type: null,
  query: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'View',
    plural: 'Views',
    route: '/views'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer'
      },
      {
        dataIndex: 'name',
        title: 'Name',
        sorter: true,
        width: 150,
        type: 'string'
      },
      {
        dataIndex: 'published',
        title: 'Published',
        sorter: true,
        width: 150,
        render: (val, record, index) => (val ? 'Published' : 'Unpublished'),
        type: 'boolean'
      }
    ]
  }),
  schemaName: 'views',
  recordType: View,
  modelSchema: viewSchema
}

const modelEffects = config => ({
  create (item, rootState) {
    const { apiPath, resourceName, modelSchema } = config
    const query = rootState[item.type].get('params').toJS()
    const userId = rootState.auth.getIn(['user', 'id'])
    return api
      .post(apiPath, { ...item, query, userId })
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Created Successfully`)
        this.merge(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  update (id, rootState) {
    const { apiPath, resourceName, modelSchema } = config
    const view = rootState.views.getIn(['list', parseInt(id)])
    const query = rootState[view.type].get('params')
    const upView = view.set('query', query).toJS()
    return api
      .post(`${apiPath}/${id}`, upView)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Updated Successfully`)
        this.merge(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  rename (item) {
    const { apiPath, resourceName, modelSchema } = config
    return api
      .post(`${apiPath}/${item.id}`, item)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Updated Successfully`)
        this.merge(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  async fetchByType (type, rootState) {
    const { listSchema, apiPath } = config
    const res = await api.get(`${apiPath}?search=${type}&searchFields=type`)
    const normalized = normalize(res.data, listSchema)
    this.merge(normalized)
    return res.data
  }
})

const modelReducers = config => ({})

const modelSelectors = (slice, createSelector, hasProps) => ({
  getViewsByType () {
    return createSelector(
      this.getItems,
      (state, type) => type,
      (items, type) => items.filter(d => d.type === type)
    )
  }
})

const model = extendFactory(
  modelConfig,
  modelReducers,
  modelEffects,
  modelSelectors
)

export default model
