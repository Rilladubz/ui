import { Record, fromJS } from 'immutable'
import { schema } from 'normalizr'

import { extendFactory } from 'models/base'
import { permissionSchema } from './permissions'

export const roleSchema = new schema.Entity('roles', {
  permissions: [permissionSchema]
})

const Role = new Record({
  id: null,
  name: null,
  permissions: fromJS([]),
  // commented out this line there's some problem with this column in the database
  // guard: 'api',
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Role',
    plural: 'Roles',
    route: '/roles'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'name',
        title: 'Name',
        sorter: true,
        width: 200,
        type: 'string',
        filter: true
      }
    ]
  }),
  resourceForm: deps => ({
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          title: 'Name',
          minLength: 1
        },
        permissions: {
          type: 'array',
          uniqueItems: true,
          title: 'Permissions',
          items: {
            type: 'number',
            anyOf: deps.permissions.map(r => ({
              type: 'number',
              enum: [r.id],
              title: r.name
            }))
          },
          multiple: true
        }
      },
      required: ['name']
    },
    uiSchema: {
      name: {
        'ui:emptyValue': '',
        'ui:placeholder': 'Name',
        'ui:options': {
          wrapperCol: 8
        }
      },
      permissions: {
        'ui:emptyValue': [],
        'ui:options': {
          mode: 'multiple',
          wrapperCol: 12,
          orderable: false,
          placeholder: 'Choose permissions(s)'
        }
      }
    }
  }),
  schemaName: 'roles',
  recordType: Role,
  modelSchema: roleSchema
}
const model = extendFactory(modelConfig)

export default model
