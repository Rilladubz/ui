import { Record } from 'immutable'

import { extendFactory } from 'models/base'

const Notif = new Record({
  id: null,
  data: null,
  createdAt: null,
  updatedAt: null
})

const modelConfig = {
  resourceName: {
    singular: 'Notification',
    plural: 'Notifications'
  },
  schemaName: 'notifs',
  recordType: Notif
}
const model = extendFactory(modelConfig)

export default model
