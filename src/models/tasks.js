import { Record, fromJS } from 'immutable'
import { normalize } from 'normalizr'
import { toastr } from 'react-redux-toastr'
import moment from 'moment'
import momentJdate from 'moment-jdateformatparser'

import store from 'store'
import { extendFactory } from 'models/base'
import api from 'middleware/camunda'
import { buildCamundaParams } from 'utils/filters'

const Task = new Record({
  id: null,
  name: null,
  assignee: null,
  due: null,
  owner: null,
  delegationState: null,
  taskDefinitionKey: null,
  description: null,
  created: null,
  followUp: null,
  priority: null,
  parentTaskId: null,
  caseInstanceId: null,
  tenantId: null,
  formVariables: fromJS({})
})

export const modelConfig = {
  resourceName: {
    singular: 'Task',
    plural: 'Tasks',
    route: '/tasks'
  },
  resourceList: state => ({
    idForItem: (item, index) => index,
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        width: 300,
        sorter: true,
        type: 'integer'
      },
      {
        dataIndex: 'name',
        title: 'Name',
        width: 150,
        sorter: true,
        filter: true
      },
      {
        dataIndex: 'assignee',
        title: 'Assignee',
        width: 150,
        sorter: true,
        filter: true,
        render: (val, record, index) => {
          let user = store.select.users
            .getList(store.getState())
            .get(parseInt(val))
          return user ? `${user.firstName} ${user.lastName}` : 'Unassigned'
        }
      },
      {
        dataIndex: 'delegationState',
        width: 100,
        title: 'Status',
        sorter: true,
        filter: true,
        render: (val, record, index) => val || 'Incomplete'
      }
    ]
  }),
  resourceForm: deps => ({
    beforeSave: (deps, form) => {
      form.due = form.due
        ? moment(form.due)
          .formatWithJDF("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
          .toString()
        : null
      form.followUp = form.followUp
        ? moment(form.followUp)
          .formatWithJDF("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
          .toString()
        : null
      return form
    },
    beforeEdit: (deps, form) => {
      form.due = form.due ? moment(form.due).format('L LT') : null
      form.followUp = form.followUp
        ? moment(form.followUp).format('L LT')
        : null
      return form
    },
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          title: 'Name',
          minLength: 1
        },
        description: {
          type: 'string',
          title: 'Description',
          minLength: 1
        },
        assignee: {
          type: 'number',
          title: 'Assignee',
          anyOf: deps.users.map(r => ({
            type: 'number',
            enum: [r.id],
            title: `${r.firstName} ${r.lastName}`
          }))
        },
        priority: {
          type: 'number',
          title: 'Priority',
          minLength: 1
        }
      },
      required: ['name']
    },
    uiSchema: {
      name: {
        'ui:emptyValue': '',
        'ui:placeholder': 'First Name',
        'ui:options': {
          wrapperCol: 12,
          placeholder: 'Name'
        }
      },
      description: {
        'ui:emptyValue': [],
        'ui:options': {
          wrapperCol: 12,
          placeholder: 'Description'
        }
      },
      assignee: {
        'ui:emptyValue': [],
        'ui:options': {
          wrapperCol: 12,
          placeholder: 'Choose assigneee'
        }
      },
      priority: {
        'ui:emptyValue': '',
        'ui:placeholder': 'E-mail',
        'ui:options': {
          wrapperCol: 12,
          placeholder: 'Priority'
        }
      }
    }
  }),
  apiPath: 'task',
  schemaName: 'tasks',
  recordType: Task
}

const modelEffects = config => ({
  async fetch ({ id, params }) {
    const { modelSchema } = config
    const res = await api.get(`task/${id}`, { params })
    this.merge(normalize(res.data, modelSchema))
    return res.data
  },
  async fetchAll (params = null, rootState) {
    const { listSchema, apiPath, schemaName } = config
    const _params = params || buildCamundaParams(rootState[schemaName])
    const res = await api.get(`${apiPath}?${_params}`)
    const reset = _params !== ''
    this.merge({ reset, ...normalize(res.data, listSchema) })
    return res.data
  },
  create (item) {
    const { apiPath, resourceName, modelSchema } = config
    return api
      .post(`${apiPath}/create`, item)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Created Successfully`)
        this.merge(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  update (item) {
    const { apiPath, resourceName, modelSchema } = config
    return api
      .put(`${apiPath}/${item.id}`, item)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Updated Successfully`)
        this.merge(normalize(item, modelSchema))
        return item
      })
      .catch(e => console.error(e))
  },
  claimTask (taskId, rootState) {
    const body = { userId: rootState.auth.user.id }
    return api
      .post(`/task/${taskId}/claim`, JSON.stringify(body))
      .then(({ data }) => {
        toastr.success('Task Claimed Successfully')
        this.claim({ userId: body.userId, taskId })
        return data
      })
      .catch(e => console.error(e))
  },
  complete ({ taskId, data }, rootState) {
    const body = { ...data }
    return api
      .post(`/task/${taskId}/complete`, JSON.stringify(body))
      .then(({ data }) => {
        toastr.success('Task Completed Successfully')
        this.delete(taskId)
        return data
      })
      .catch(e => console.error(e))
  }
})

const modelReducers = config => ({
  claim: (state, payload) => {
    const { userId, taskId } = payload
    let task = state.getIn(['list', `${taskId}`])
    task = task.set('assignee', userId)
    return state.setIn(['list', `${taskId}`], task)
  }
})

const modelSelectors = (slice, createSelector, hasProps) => ({
  getMyTasks (models) {
    return createSelector(
      this.getItems,
      models.auth.getAuthUser,
      (items, user) => items.filter(t => t.assignee === `${user.id}`)
    )
  },
  getPendingItems () {
    return createSelector(
      this.getItems,
      items => items.filter(d => d.documentTypeId === null)
    )
  },
  getOne () {
    return createSelector(
      this.getList,
      (state, props) => props.id,
      (items, id) => items.get(id)
    )
  }
})

const model = extendFactory(
  modelConfig,
  modelReducers,
  modelEffects,
  modelSelectors
)

export default model
