import { Record } from 'immutable'

import { extendFactory } from 'models/base'

const RecordType = new Record({
  id: null,
  name: null,
  description: null,
  meta: {
    listFields: []
  },
  schema: null,
  uiSchema: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Record Type',
    plural: 'Record Types',
    route: '/record-types'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'name',
        title: 'Name',
        sorter: true,
        width: 150,
        type: 'string',
        filter: true
      },
      {
        dataIndex: 'description',
        title: 'Description',
        sorter: true,
        width: 150,
        type: 'string',
        filter: true
      }
    ]
  }),
  resourceForm: {
    init: {
      name: '',
      description: ''
    },
    validations: [
      { name: 'name', rule: (form, v) => v !== '' },
      { name: 'description', rule: (form, v) => v !== '' }
    ],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        },
        {
          name: 'description',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Description',
            label: 'Description',
            required: true
          }
        }
      ]
    ]
  },
  schemaName: 'recordTypes',
  apiPath: 'record_types',
  recordType: RecordType
}

const model = extendFactory(modelConfig)

export default model
