import { Record, fromJS } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import { ucFirst } from 'utils/render'
import store from './index'
import { extendFactory } from 'models/base'
import api from 'middleware/camunda'

export const activitySchema = new schema.Entity('activityTree')
export const varSchema = new schema.Entity('variables')
export const processSchema = new schema.Entity(
  'processes',
  {},
  {
    idAttribute: 'processInstanceId'
  }
)

export const Process = new Record({
  id: null,
  processInstanceId: null,
  workflowId: null,
  definitionId: null,
  ended: null,
  businessKey: null,
  caseInstanceId: null,
  variables: null,
  activity: null
})

const modelConfig = {
  resourceName: {
    singular: 'Process',
    plural: 'Processes'
  },
  schemaName: 'processes',
  modelSchema: processSchema,
  recordType: Process
}

const modelEffects = config => ({
  async fetch (id) {
    const { modelSchema } = config
    const res = await api.get(`/process-instance/${id}`)
    this.merge(normalize(res.data, modelSchema))
    return res.data
  },
  async fetchAll () {
    const { listSchema } = config
    const res = await api.get('/process-instance')
    this.merge(normalize(res.data, listSchema))
    return res.data
  },
  fetchActivityTree (id) {
    return api
      .get(`/process-instance/${id}/activity-instances`)
      .then(({ data }) => {
        this.receiveActivities(data)
        return data
      })
      .catch(e => console.error(e))
  },
  fetchVariables (id) {
    return api
      .get(`/process-instance/${id}/variables`)
      .then(({ data }) => {
        this.receiveVars({ data, id })
        return data
      })
      .catch(e => console.error(e))
  }
})

const modelReducers = config => ({
  receiveActivities: (state, data) => {
    if (data) {
      const activities = fromJS(data)
      return state.setIn(
        ['list', `${activities.get('id')}`, 'activity'],
        activities
      )
    }
    return state
  },
  receiveVars: (state, payload) => {
    const { data, id } = payload
    const vars = fromJS(data)
    return state.setIn(['list', `${id}`, 'variables'], vars)
  }
})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
