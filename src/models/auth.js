import { Record } from 'immutable'
import api from 'middleware/api'
import { notification } from 'antd'

const Auth = new Record({
  isAuthenticating: false,
  isAuthenticated: false,
  enterCode: false,
  resetPassword: false,
  loginError: null,
  signupError: null,
  token: null,
  user: null
})

const User = new Record({
  id: null,
  firstName: null,
  lastName: null,
  password: null,
  email: null,
  roles: null,
  createdAt: null,
  updatedAt: null,
  token: null,
  notifications: null
})

export default {
  state: new Auth(),
  reducers: {
    isAuthenticating: state => state.set('isAuthenticating', true),
    loginSuccess: (state, payload) => {
      localStorage.setItem('token', payload.token)
      return state
        .set('isAuthenticating', false)
        .set('token', payload.token)
        .set('user', new User(payload.user))
        .set('isAuthenticated', true)
        .set('signupError', null)
        .set('loginError', null)
    },
    loginError: (state, payload) => {
      return state.set('isAuthenticating', false).set('loginError', payload)
    },
    signupError: (state, payload) => {
      const { error } = payload
      const msg =
        error.status_code === 422
          ? 'This e-mail is already taken.'
          : error.message
      return state.set('isAuthenticating', false).set('signupError', msg)
    },
    logoutUser: state => {
      localStorage.removeItem('token')
      return state
        .set('isAuthenticated', false)
        .set('token', null)
        .set('user', null)
        .set('loginError', null)
    },
    receiveAccountInfo: (state, payload) => {
      return state
        .set('isAuthenticating', false)
        .set('user', new User(payload.user))
        .set('isAuthenticated', true)
    }
  },
  effects: {
    loginWithToken () {
      this.isAuthenticating()
      return api
        .get('/auth/me')
        .then(({ data }) => {
          this.receiveAccountInfo(data)
          return data
        })
        .catch(e => this.loginError('Session expired.'))
    },
    login (payload) {
      const body = JSON.stringify({
        email: payload.email,
        password: payload.password
      })
      return api
        .post('/auth/login', body)
        .then(({ data }) => {
          this.loginSuccess(data)
        })
        .then(() => {
          notification['success']({
            message: 'Login Success',
            duration: 2
          })
        })
        .catch(e => {
          this.loginError('Invalid credentials.')
          notification['error']({
            message: 'Login error',
            description: 'Invalid credentials',
            duration: 2
          })
        })
    },
    signup (payload) {
      const body = JSON.stringify(payload)
      return api
        .post('/auth/signup', body)
        .then(({ data }) => {
          this.loginSuccess(data)
        })
        .catch(e => this.signupError(e.response.data))
    },
    logout () {
      this.logoutUser()
    }
  },
  selectors: (slice, createSelector, hasProps) => ({
    getAuthUser: () =>
      createSelector(
        slice,
        s => s.get('user')
      )
  })
}
