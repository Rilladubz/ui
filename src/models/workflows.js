import { Record, fromJS, OrderedMap } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import { extendFactory } from 'models/base'
import api from 'middleware/api'

export const workflowSchema = new schema.Entity('workflows')

const Workflow = new Record({
  id: null,
  name: null,
  bpmnXml: null,
  bpmnId: null,
  published: null,
  documentTypeId: null,
  deploymentId: null,
  options: {
    startInstanceOnUpload: false
  },
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Workflow',
    plural: 'Workflows',
    route: '/workflows'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'name',
        title: 'Name',
        sorter: true,
        type: 'string',
        filter: true
      },
      {
        dataIndex: 'published',
        title: 'Published',
        sorter: true,
        width: 150,
        render: (val, record, index) => (val ? 'Published' : 'Unpublished'),
        type: 'boolean',
        filter: true
      }
    ]
  }),
  schemaName: 'workflows',
  recordType: Workflow,
  modelSchema: workflowSchema
}

const modelEffects = config => ({
  publish (id) {
    const { apiPath, modelSchema } = config
    return api
      .get(`${apiPath}/publish/${id}`)
      .then(({ data }) => {
        toastr.success(`Workflow Published Successfully`)
        this.merge(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  unpublish (id) {
    const { apiPath, modelSchema } = config
    return api
      .get(`${apiPath}/unpublish/${id}`)
      .then(({ data }) => {
        toastr.success(`Workflow Unpublished Successfully`)
        this.merge(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  duplicate (id) {
    const { apiPath, modelSchema } = config
    return api
      .get(`${apiPath}/duplicate/${id}`)
      .then(({ data }) => {
        toastr.success(`Workflow Duplicated Successfully`)
        this.merge(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  }
})

const modelReducers = config => ({})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
