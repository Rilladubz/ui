import { Record as IRecord } from 'immutable'

import { extendFactory } from 'models/base'
import store from 'store'

const Record = new IRecord({
  id: null,
  recordTypeId: null,
  data: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Record',
    plural: 'Records',
    route: '/records'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'recordTypeId',
        title: 'Record Type',
        render: v =>
          store.select.recordTypes.getList(store.getState()).getIn([v, 'name']),
        sorter: true,
        width: 200,
        type: 'integer',
        filter: true
      }
    ]
  }),
  resourceForm: {
    init: {
      recordTypeId: '',
      data: ''
    },
    validations: [{ name: 'recordTypeId', rule: (form, v) => v !== '' }],
    fields: [
      [
        {
          name: 'recordTypeId',
          component: 'Dropdown',
          getOptions: deps =>
            deps.recordTypes.map(r => ({
              key: r.id,
              text: r.name,
              value: r.id
            })),
          props: {
            search: true,
            selection: true,
            placeholder: 'Choose a record type',
            label: 'Record Type',
            clearable: true
          }
        },
        {
          name: 'data',
          component: 'JSONSchema',
          props: {}
        }
      ]
    ]
  },
  schemaName: 'records',
  recordType: Record
}
const model = extendFactory(modelConfig)

export default model
