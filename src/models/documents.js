import { Record } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import api from 'middleware/api'
import store from 'store'
import { extendFactory } from 'models/base'
import { DocumentTypeSchema } from './documentTypes'
import { processSchema } from './processes'
import { buildParams } from 'utils/filters'

export const DocumentSchema = new schema.Entity('documents', {
  documentType: DocumentTypeSchema,
  processes: [processSchema]
})

const Document = new Record({
  id: null,
  filename: null,
  extension: null,
  mimeType: null,
  documentTypeId: null,
  documentType: null,
  processes: [],
  url: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Document',
    plural: 'Documents',
    route: '/documents'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'documentTypeId',
        title: 'Document Type',
        sorter: true,
        width: 150,
        filter: true,
        render: v => {
          const items = store.select.documentTypes.getList(store.getState())
          return items.getIn([v, 'name'])
        },
        type: 'integer'
      },
      {
        dataIndex: 'filename',
        title: 'Filename',
        sorter: true,
        width: 100,
        render: (v, r) => `${v}.${r.extension}`,
        type: 'string',
        filter: true
      },
      {
        dataIndex: 'mimeType',
        title: 'MIME Type',
        sorter: true,
        width: 150,
        type: 'string',
        filter: true
      }
    ]
  }),
  resourceForm: {
    init: {
      name: '',
      properties: {
        fileTypes: []
      },
      description: ''
    },
    validations: [
      { name: 'name', rule: (form, v) => v !== '' },
      { name: 'description', rule: (form, v) => v !== '' }
    ],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        },
        {
          name: 'description',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Description',
            label: 'Description',
            required: true
          }
        }
      ],
      [
        {
          name: 'properties',
          component: 'Dropdown',
          getOptions: (deps, form) =>
            deps.fileTypes[form.fileType].map((r, k) => ({
              key: k,
              text: r.name,
              value: k
            })),
          props: {
            search: true,
            selection: true,
            required: true,
            placeholder: 'Choose file type',
            label: 'File Type'
          }
        }
      ]
    ]
  },
  schemaName: 'documents',
  modelSchema: DocumentSchema,
  recordType: Document,
  apiPath: 'documents'
}

const modelReducers = config => ({})

const modelEffects = config => ({
  create (item) {
    const { apiPath, resourceName, listSchema } = config
    return api
      .post(apiPath, item)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Created Successfully`)
        this.merge(normalize(data, listSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  async fetchPending (id = null) {
    const { listSchema } = config
    const url =
      id === null ? '/documents/pending' : `/documents/${id}?pending=true`
    const res = await api.get(url)
    const normalized = normalize(res.data, listSchema)
    this.merge(normalized)
    return res.data
  },
  async fetchAll (params = null, rootState) {
    const { listSchema, apiPath, schemaName } = config
    const _params = params || buildParams(rootState[schemaName])
    const res = await api.get(`${apiPath}?${_params}`)
    const reset = _params !== ''
    const normalized = normalize(res.data, listSchema)
    this.merge({ reset, ...normalized })
    store.dispatch.documentTypes.merge(normalized)
    return res.data
  },
  async fetch ({ id, params }) {
    const { modelSchema, apiPath } = config
    const res = await api.get(`${apiPath}/${id}`, { params })
    const normalized = normalize(res.data, modelSchema)
    this.merge(normalized)
    store.dispatch.documentTypes.merge(normalized)
    store.dispatch.processes.merge(normalized)
    return res.data
  },
  removePending (id) {
    const { apiPath, resourceName } = config
    return api
      .delete(`${apiPath}/${id}?pending=true`)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Deleted Successfully`)
        this.delete(id)
        return data
      })
      .catch(e => console.error(e))
  }
})

const modelSelectors = (slice, createSelector, hasProps) => ({
  getFilteredItems () {
    return createSelector(
      this.getItems,
      items => items.filter(d => d.documentTypeId !== null)
    )
  },
  getPendingItems () {
    return createSelector(
      this.getItems,
      items => items.filter(d => d.documentTypeId === null)
    )
  }
})

const model = extendFactory(
  modelConfig,
  modelReducers,
  modelEffects,
  modelSelectors
)

export default model
