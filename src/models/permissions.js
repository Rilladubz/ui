import { Record } from 'immutable'
import { schema } from 'normalizr'

import { extendFactory } from 'models/base'

export const permissionSchema = new schema.Entity('permissions')

const Permission = new Record({
  id: null,
  name: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Permission',
    plural: 'Permissions',
    route: '/permissions'
  },
  resourceList: state => ({
    columns: [
      {
        dataIndex: 'id',
        title: 'ID',
        sorter: true,
        width: 50,
        type: 'integer',
        filter: true
      },
      {
        dataIndex: 'name',
        title: 'Name',
        sorter: true,
        width: 150,
        type: 'string',
        filter: true
      }
    ]
  }),
  resourceForm: deps => ({
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          title: 'Name',
          minLength: 1
        }
      },
      required: ['name']
    },
    uiSchema: {
      name: {
        'ui:emptyValue': '',
        'ui:placeholder': 'Name',
        'ui:options': {
          wrapperCol: 8
        }
      }
    }
  }),
  schemaName: 'permissions',
  recordType: Permission,
  modelSchema: permissionSchema
}
const model = extendFactory(modelConfig)

export default model
