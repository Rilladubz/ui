const { override, fixBabelImports, addLessLoader } = require('customize-cra')
const antdVars = require('./antd-override.json')

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true
  }),
  addLessLoader({
    modifyVars: antdVars,
    javascriptEnabled: true
  })
)
const rewireReactHotLoader = require('react-app-rewire-hot-loader')

module.exports = function override (config, env) {
  config = rewireReactHotLoader(config, env)
  return config
}
